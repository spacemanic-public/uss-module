EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1900 4350 2    50   Input ~ 0
2.2
Text GLabel 1900 4550 2    50   Input ~ 0
2.6
Text GLabel 1900 4750 2    50   Input ~ 0
2.10
Text GLabel 1900 4850 2    50   Input ~ 0
2.12
Text GLabel 1900 4650 2    50   Input ~ 0
2.8
Text GLabel 1400 4750 0    50   Input ~ 0
2.9
Text GLabel 1400 4450 0    50   Input ~ 0
2.3
Text GLabel 1400 4550 0    50   Input ~ 0
2.5
Text GLabel 1400 4650 0    50   Input ~ 0
2.7
Text GLabel 1400 4850 0    50   Input ~ 0
2.11
Text GLabel 1900 4450 2    50   Input ~ 0
2.4
Text GLabel 1900 3800 2    50   Input ~ 0
1.52
Text GLabel 1900 3600 2    50   Input ~ 0
1.48
Text GLabel 1900 3700 2    50   Input ~ 0
1.50
Text GLabel 1400 4350 0    50   Input ~ 0
2.1
Text GLabel 1400 3800 0    50   Input ~ 0
1.51
Text GLabel 1400 3600 0    50   Input ~ 0
1.47
Text GLabel 1400 3700 0    50   Input ~ 0
1.49
Text GLabel 1900 3400 2    50   Input ~ 0
1.44
Text GLabel 1900 3300 2    50   Input ~ 0
1.42
Text GLabel 1900 3100 2    50   Input ~ 0
1.38
Text GLabel 1900 3200 2    50   Input ~ 0
1.40
Text GLabel 1900 3500 2    50   Input ~ 0
1.46
Text GLabel 2800 3800 0    50   Input ~ 0
1.51
Text GLabel 2800 4350 0    50   Input ~ 0
2.1
Text GLabel 2800 4450 0    50   Input ~ 0
2.3
Text GLabel 2800 4550 0    50   Input ~ 0
2.5
Text GLabel 3300 4450 2    50   Input ~ 0
2.4
Text GLabel 2800 3500 0    50   Input ~ 0
1.45
Text GLabel 2800 3700 0    50   Input ~ 0
1.49
Text GLabel 2800 3600 0    50   Input ~ 0
1.47
Text GLabel 2800 3400 0    50   Input ~ 0
1.43
Text GLabel 2800 3300 0    50   Input ~ 0
1.41
Text GLabel 3300 3300 2    50   Input ~ 0
1.42
Text GLabel 3300 3200 2    50   Input ~ 0
1.40
Text GLabel 2800 3200 0    50   Input ~ 0
1.39
Text GLabel 3300 3100 2    50   Input ~ 0
1.38
Text GLabel 3300 3500 2    50   Input ~ 0
1.46
Text GLabel 3300 3800 2    50   Input ~ 0
1.52
Text GLabel 3300 3700 2    50   Input ~ 0
1.50
Text GLabel 3300 3600 2    50   Input ~ 0
1.48
Text GLabel 3300 3400 2    50   Input ~ 0
1.44
Text GLabel 1400 5350 0    50   Input ~ 0
2.21
Text GLabel 1400 5250 0    50   Input ~ 0
2.19
Text GLabel 1400 5150 0    50   Input ~ 0
2.17
Text GLabel 1400 5050 0    50   Input ~ 0
2.15
Text GLabel 1400 4950 0    50   Input ~ 0
2.13
Text GLabel 1400 6850 0    50   Input ~ 0
2.51
Text GLabel 1400 6750 0    50   Input ~ 0
2.49
Text GLabel 1400 6650 0    50   Input ~ 0
2.47
Text GLabel 1400 6550 0    50   Input ~ 0
2.45
Text GLabel 1400 6450 0    50   Input ~ 0
2.43
Text GLabel 1400 6350 0    50   Input ~ 0
2.41
Text GLabel 1400 6250 0    50   Input ~ 0
2.39
Text GLabel 1400 6150 0    50   Input ~ 0
2.37
Text GLabel 1400 5650 0    50   Input ~ 0
2.27
Text GLabel 1400 5450 0    50   Input ~ 0
2.23
Text GLabel 1400 5550 0    50   Input ~ 0
2.25
Text GLabel 1400 5850 0    50   Input ~ 0
2.31
Text GLabel 1400 5750 0    50   Input ~ 0
2.29
Text GLabel 1400 5950 0    50   Input ~ 0
2.33
Text GLabel 1400 6050 0    50   Input ~ 0
2.35
Text GLabel 1900 6150 2    50   Input ~ 0
2.38
Text GLabel 1900 6250 2    50   Input ~ 0
2.40
Text GLabel 1900 5850 2    50   Input ~ 0
2.32
Text GLabel 1900 5750 2    50   Input ~ 0
2.30
$Comp
L MLAB_HEADER:HEADER_2x26 J2
U 1 1 5D197155
P 1650 5600
F 0 "J2" H 1650 7203 60  0000 C CNN
F 1 "HEADER_2x26" H 1650 7097 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 1650 6991 60  0001 C CNN
F 3 "" H 1650 6850 60  0000 C CNN
	1    1650 5600
	1    0    0    -1  
$EndComp
Text GLabel 1900 5250 2    50   Input ~ 0
2.20
Text GLabel 1900 5350 2    50   Input ~ 0
2.22
Text GLabel 1900 5150 2    50   Input ~ 0
2.18
Text GLabel 1900 5050 2    50   Input ~ 0
2.16
Text GLabel 1900 4950 2    50   Input ~ 0
2.14
Text GLabel 1900 5450 2    50   Input ~ 0
2.24
Text GLabel 1900 5550 2    50   Input ~ 0
2.26
Text GLabel 1900 5650 2    50   Input ~ 0
2.28
Text GLabel 2800 6150 0    50   Input ~ 0
2.37
Text GLabel 2800 6250 0    50   Input ~ 0
2.39
Text GLabel 2800 6350 0    50   Input ~ 0
2.41
Text GLabel 2800 6450 0    50   Input ~ 0
2.43
Text GLabel 2800 6550 0    50   Input ~ 0
2.45
Text GLabel 2800 5850 0    50   Input ~ 0
2.31
Text GLabel 2800 6650 0    50   Input ~ 0
2.47
Text GLabel 2800 6750 0    50   Input ~ 0
2.49
Text GLabel 2800 6850 0    50   Input ~ 0
2.51
Text GLabel 3300 6850 2    50   Input ~ 0
2.52
Text GLabel 3300 6750 2    50   Input ~ 0
2.50
Text GLabel 3300 6650 2    50   Input ~ 0
2.48
Text GLabel 3300 6550 2    50   Input ~ 0
2.46
Text GLabel 3300 6450 2    50   Input ~ 0
2.44
Text GLabel 3300 6350 2    50   Input ~ 0
2.42
Text GLabel 2800 5950 0    50   Input ~ 0
2.33
Text GLabel 2800 6050 0    50   Input ~ 0
2.35
Text GLabel 1900 6350 2    50   Input ~ 0
2.42
Text GLabel 1900 5950 2    50   Input ~ 0
2.34
Text GLabel 1900 6050 2    50   Input ~ 0
2.36
Text GLabel 1900 6450 2    50   Input ~ 0
2.44
Text GLabel 1900 6550 2    50   Input ~ 0
2.46
Text GLabel 1900 6650 2    50   Input ~ 0
2.48
Text GLabel 1900 6750 2    50   Input ~ 0
2.50
Text GLabel 1900 6850 2    50   Input ~ 0
2.52
Text GLabel 3300 6250 2    50   Input ~ 0
2.40
Text GLabel 3300 6150 2    50   Input ~ 0
2.38
Text GLabel 3300 6050 2    50   Input ~ 0
2.36
Text GLabel 3300 5950 2    50   Input ~ 0
2.34
Text GLabel 3300 5850 2    50   Input ~ 0
2.32
Text GLabel 2800 5750 0    50   Input ~ 0
2.29
Text GLabel 2800 5650 0    50   Input ~ 0
2.27
Text GLabel 2800 5450 0    50   Input ~ 0
2.23
Text GLabel 2800 5550 0    50   Input ~ 0
2.25
Text GLabel 3300 5750 2    50   Input ~ 0
2.30
$Comp
L MLAB_HEADER:HEADER_2x26 J4
U 1 1 5E34ACC1
P 3050 5600
F 0 "J4" H 3050 7203 60  0000 C CNN
F 1 "HEADER_2x26" H 3050 7097 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3050 6991 60  0001 C CNN
F 3 "" H 3050 6850 60  0000 C CNN
	1    3050 5600
	1    0    0    -1  
$EndComp
Text GLabel 2800 5250 0    50   Input ~ 0
2.19
Text GLabel 2800 5350 0    50   Input ~ 0
2.21
Text GLabel 2800 5150 0    50   Input ~ 0
2.17
Text GLabel 2800 5050 0    50   Input ~ 0
2.15
Text GLabel 2800 4950 0    50   Input ~ 0
2.13
Text GLabel 2800 4750 0    50   Input ~ 0
2.9
Text GLabel 2800 4850 0    50   Input ~ 0
2.11
Text GLabel 2800 4650 0    50   Input ~ 0
2.7
Text GLabel 3300 5350 2    50   Input ~ 0
2.22
Text GLabel 3300 5450 2    50   Input ~ 0
2.24
Text GLabel 3300 5250 2    50   Input ~ 0
2.20
Text GLabel 3300 5550 2    50   Input ~ 0
2.26
Text GLabel 3300 5150 2    50   Input ~ 0
2.18
Text GLabel 3300 5650 2    50   Input ~ 0
2.28
Text GLabel 3300 4950 2    50   Input ~ 0
2.14
Text GLabel 3300 5050 2    50   Input ~ 0
2.16
Text GLabel 3300 4350 2    50   Input ~ 0
2.2
Text GLabel 3300 4550 2    50   Input ~ 0
2.6
Text GLabel 3300 4650 2    50   Input ~ 0
2.8
Text GLabel 3300 4750 2    50   Input ~ 0
2.10
Text GLabel 3300 4850 2    50   Input ~ 0
2.12
Text GLabel 1400 3500 0    50   Input ~ 0
1.45
Text GLabel 1400 3400 0    50   Input ~ 0
1.43
Text GLabel 1400 3300 0    50   Input ~ 0
1.41
Text GLabel 1400 3100 0    50   Input ~ 0
1.37
Text GLabel 1400 3200 0    50   Input ~ 0
1.39
Text GLabel 1400 2900 0    50   Input ~ 0
1.33
Text GLabel 1400 3000 0    50   Input ~ 0
1.35
Text GLabel 1900 3000 2    50   Input ~ 0
1.36
Text GLabel 1900 2900 2    50   Input ~ 0
1.34
Text GLabel 1400 2800 0    50   Input ~ 0
1.31
Text GLabel 1900 2800 2    50   Input ~ 0
1.32
Text GLabel 1900 2700 2    50   Input ~ 0
1.30
Text GLabel 1900 2600 2    50   Input ~ 0
1.28
Text GLabel 1900 2400 2    50   Input ~ 0
1.24
Text GLabel 1900 2500 2    50   Input ~ 0
1.26
Text GLabel 1900 2300 2    50   Input ~ 0
1.22
Text GLabel 1400 2700 0    50   Input ~ 0
1.29
Text GLabel 1400 2600 0    50   Input ~ 0
1.27
Text GLabel 1400 2500 0    50   Input ~ 0
1.25
Text GLabel 1400 2400 0    50   Input ~ 0
1.23
Text GLabel 1400 2300 0    50   Input ~ 0
1.21
Text GLabel 1400 2200 0    50   Input ~ 0
1.19
Text GLabel 1400 1900 0    50   Input ~ 0
1.13
Text GLabel 1400 2000 0    50   Input ~ 0
1.15
Text GLabel 1400 2100 0    50   Input ~ 0
1.17
Text GLabel 1400 1800 0    50   Input ~ 0
1.11
Text GLabel 1900 2200 2    50   Input ~ 0
1.20
Text GLabel 1900 2100 2    50   Input ~ 0
1.18
Text GLabel 1900 1800 2    50   Input ~ 0
1.12
Text GLabel 1900 1900 2    50   Input ~ 0
1.14
Text GLabel 1900 2000 2    50   Input ~ 0
1.16
Text GLabel 1400 1500 0    50   Input ~ 0
1.5
Text GLabel 1900 1400 2    50   Input ~ 0
1.4
Text GLabel 1900 1300 2    50   Input ~ 0
1.2
Text GLabel 1400 1300 0    50   Input ~ 0
1.1
Text GLabel 1400 1700 0    50   Input ~ 0
1.9
Text GLabel 1400 1400 0    50   Input ~ 0
1.3
Text GLabel 1400 1600 0    50   Input ~ 0
1.7
$Comp
L MLAB_HEADER:HEADER_2x26 J1
U 1 1 5D194D38
P 1650 2550
F 0 "J1" H 1650 4153 60  0000 C CNN
F 1 "HEADER_2x26" H 1650 4047 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 1650 3941 60  0001 C CNN
F 3 "" H 1650 3800 60  0000 C CNN
	1    1650 2550
	1    0    0    -1  
$EndComp
Text GLabel 1900 1500 2    50   Input ~ 0
1.6
Text GLabel 1900 1600 2    50   Input ~ 0
1.8
Text GLabel 1900 1700 2    50   Input ~ 0
1.10
Text GLabel 2800 1800 0    50   Input ~ 0
1.11
Text GLabel 2800 1900 0    50   Input ~ 0
1.13
Text GLabel 2800 2600 0    50   Input ~ 0
1.27
Text GLabel 2800 2500 0    50   Input ~ 0
1.25
Text GLabel 2800 2400 0    50   Input ~ 0
1.23
Text GLabel 2800 2300 0    50   Input ~ 0
1.21
Text GLabel 2800 2200 0    50   Input ~ 0
1.19
Text GLabel 2800 2100 0    50   Input ~ 0
1.17
Text GLabel 2800 2000 0    50   Input ~ 0
1.15
Text GLabel 2800 1400 0    50   Input ~ 0
1.3
Text GLabel 2800 1500 0    50   Input ~ 0
1.5
Text GLabel 2800 1600 0    50   Input ~ 0
1.7
Text GLabel 2800 1700 0    50   Input ~ 0
1.9
Text GLabel 2800 1300 0    50   Input ~ 0
1.1
Text GLabel 2800 3000 0    50   Input ~ 0
1.35
Text GLabel 2800 3100 0    50   Input ~ 0
1.37
Text GLabel 2800 2900 0    50   Input ~ 0
1.33
Text GLabel 2800 2800 0    50   Input ~ 0
1.31
Text GLabel 2800 2700 0    50   Input ~ 0
1.29
$Comp
L MLAB_HEADER:HEADER_2x26 J3
U 1 1 5E34AA3A
P 3050 2550
F 0 "J3" H 3050 4153 60  0000 C CNN
F 1 "HEADER_2x26" H 3050 4047 60  0000 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 3050 3941 60  0001 C CNN
F 3 "" H 3050 3800 60  0000 C CNN
	1    3050 2550
	1    0    0    -1  
$EndComp
Text GLabel 3300 2900 2    50   Input ~ 0
1.34
Text GLabel 3300 3000 2    50   Input ~ 0
1.36
Text GLabel 3300 2800 2    50   Input ~ 0
1.32
Text GLabel 3300 2700 2    50   Input ~ 0
1.30
Text GLabel 3300 2200 2    50   Input ~ 0
1.20
Text GLabel 3300 2000 2    50   Input ~ 0
1.16
Text GLabel 3300 2100 2    50   Input ~ 0
1.18
Text GLabel 3300 1900 2    50   Input ~ 0
1.14
Text GLabel 3300 1800 2    50   Input ~ 0
1.12
Text GLabel 4050 2175 0    50   Input ~ 0
2.45
Text GLabel 4050 2275 0    50   Input ~ 0
2.46
Text GLabel 3300 1700 2    50   Input ~ 0
1.10
Text GLabel 3300 1600 2    50   Input ~ 0
1.8
Text GLabel 3300 1300 2    50   Input ~ 0
1.2
Text GLabel 3300 1400 2    50   Input ~ 0
1.4
Text GLabel 3300 1500 2    50   Input ~ 0
1.6
Text GLabel 3300 2300 2    50   Input ~ 0
1.22
Text GLabel 3300 2400 2    50   Input ~ 0
1.24
Text GLabel 3300 2600 2    50   Input ~ 0
1.28
Text GLabel 3300 2500 2    50   Input ~ 0
1.26
Text GLabel 4050 2600 0    50   Input ~ 0
2.30
Text GLabel 4050 2500 0    50   Input ~ 0
2.29
Text GLabel 4050 2700 0    50   Input ~ 0
2.32
Wire Wire Line
	4050 2500 4100 2500
Wire Wire Line
	4100 2500 4100 2600
Text GLabel 5950 3550 0    50   Input ~ 0
RS485_B
Text GLabel 5950 3850 0    50   Input ~ 0
SPI_0_MOSI
Text GLabel 5950 3750 0    50   Input ~ 0
SPI_0_CS
Text GLabel 5950 3650 0    50   Input ~ 0
RS485_A
Wire Wire Line
	5850 3950 5950 3950
Text GLabel 5950 3450 0    50   Input ~ 0
I2C_0_SCL
Wire Wire Line
	5925 3150 5925 3250
Connection ~ 5925 3250
Wire Wire Line
	5925 3250 5950 3250
Wire Wire Line
	5850 3250 5925 3250
Wire Wire Line
	5850 3350 5950 3350
$Comp
L power:GND #PWR0103
U 1 1 5E4EC7B2
P 5850 3350
F 0 "#PWR0103" H 5850 3100 50  0001 C CNN
F 1 "GND" V 5855 3222 50  0000 R CNN
F 2 "" H 5850 3350 50  0001 C CNN
F 3 "" H 5850 3350 50  0001 C CNN
	1    5850 3350
	0    1    1    0   
$EndComp
Connection ~ 5925 3050
Wire Wire Line
	5950 3150 5925 3150
Wire Wire Line
	5925 3050 5950 3050
Wire Wire Line
	5925 2950 5925 3050
Wire Wire Line
	5950 2950 5925 2950
Wire Wire Line
	5850 2850 5950 2850
Wire Wire Line
	5850 3050 5925 3050
Wire Wire Line
	4700 2175 4750 2175
Wire Wire Line
	4750 2275 4700 2275
Wire Wire Line
	4750 2175 4750 2275
Wire Wire Line
	4750 2075 4750 2175
Connection ~ 4750 2175
Wire Wire Line
	4100 2700 4100 2800
Wire Wire Line
	4100 2600 4100 2700
Wire Wire Line
	4050 2700 4100 2700
Connection ~ 4100 2700
Wire Wire Line
	4050 2600 4100 2600
Wire Wire Line
	4425 2500 4425 2800
Connection ~ 4100 2600
Text GLabel 4375 2500 0    50   Input ~ 0
2.31
Wire Wire Line
	4375 2500 4425 2500
Text GLabel 4700 2275 0    50   Input ~ 0
2.28
Text GLabel 4700 2175 0    50   Input ~ 0
2.27
$Comp
L device:Jumper_NC_Small JP3
U 1 1 5E6922C9
P 4750 1975
F 0 "JP3" V 4750 2175 50  0000 R CNN
F 1 "Jumper_NC_Small" H 4750 2095 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4750 1975 50  0001 C CNN
F 3 "~" H 4750 1975 50  0001 C CNN
	1    4750 1975
	0    -1   -1   0   
$EndComp
$Comp
L device:Jumper_NC_Small JP2
U 1 1 5E68D70D
P 4425 1975
F 0 "JP2" V 4425 2175 50  0000 R CNN
F 1 "Jumper_NC_Small" H 4425 2095 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4425 1975 50  0001 C CNN
F 3 "~" H 4425 1975 50  0001 C CNN
	1    4425 1975
	0    -1   -1   0   
$EndComp
$Comp
L power:+BATT #PWR0122
U 1 1 5E526D9E
P 4100 1875
F 0 "#PWR0122" H 4100 1725 50  0001 C CNN
F 1 "+BATT" V 4115 2003 50  0000 L CNN
F 2 "" H 4100 1875 50  0001 C CNN
F 3 "" H 4100 1875 50  0001 C CNN
	1    4100 1875
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0124
U 1 1 5E6AE908
P 4425 2800
F 0 "#PWR0124" H 4425 2550 50  0001 C CNN
F 1 "GNDA" V 4425 2575 50  0000 C CNN
F 2 "" H 4425 2800 50  0001 C CNN
F 3 "" H 4425 2800 50  0001 C CNN
	1    4425 2800
	1    0    0    -1  
$EndComp
$Comp
L spacemanic_power:+3.3V_IN #PWR18
U 1 1 5E5520D0
P 4750 1875
F 0 "#PWR18" H 4750 1800 50  0001 C CNN
F 1 "+3.3V_IN" V 4736 2003 50  0000 L CNN
F 2 "" H 4750 1875 50  0001 C CNN
F 3 "" H 4750 1875 50  0001 C CNN
	1    4750 1875
	1    0    0    -1  
$EndComp
$Comp
L spacemanic_power:+5V_IN #PWR17
U 1 1 5E53E935
P 4425 1875
F 0 "#PWR17" H 4425 1800 50  0001 C CNN
F 1 "+5V_IN" V 4411 2003 50  0000 L CNN
F 2 "" H 4425 1875 50  0001 C CNN
F 3 "" H 4425 1875 50  0001 C CNN
	1    4425 1875
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2275 4050 2275
Wire Wire Line
	4050 2175 4100 2175
Wire Wire Line
	4100 2175 4100 2275
Text GLabel 4375 2175 0    50   Input ~ 0
2.25
Wire Wire Line
	4425 2175 4425 2275
Wire Wire Line
	4375 2175 4425 2175
Wire Wire Line
	4100 2075 4100 2175
Wire Wire Line
	4425 2075 4425 2175
Connection ~ 4425 2175
Connection ~ 4100 2175
Text GLabel 4375 2275 0    50   Input ~ 0
2.26
Wire Wire Line
	4425 2275 4375 2275
$Comp
L device:Jumper_NC_Small JP1
U 1 1 5E668409
P 4100 1975
F 0 "JP1" V 4100 2175 50  0000 R CNN
F 1 "Jumper_NC_Small" H 4100 2095 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4100 1975 50  0001 C CNN
F 3 "~" H 4100 1975 50  0001 C CNN
	1    4100 1975
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5E6510B7
P 4100 2800
F 0 "#PWR0123" H 4100 2550 50  0001 C CNN
F 1 "GND" V 4105 2672 50  0000 R CNN
F 2 "" H 4100 2800 50  0001 C CNN
F 3 "" H 4100 2800 50  0001 C CNN
	1    4100 2800
	1    0    0    -1  
$EndComp
Text GLabel 6450 4350 2    50   Input ~ 0
GPIO0
Text GLabel 6450 4150 2    50   Input ~ 0
CAN_L
Wire Wire Line
	6450 4250 6550 4250
Text GLabel 6450 4650 2    50   Input ~ 0
GPIO6_ADC2
Text GLabel 6450 4750 2    50   Input ~ 0
PWM1
Text GLabel 6450 4450 2    50   Input ~ 0
GPIO2
Text GLabel 6450 4550 2    50   Input ~ 0
GPIOO4_ADC0
Text GLabel 6450 4050 2    50   Input ~ 0
CAN_H
Wire Wire Line
	6450 3950 6550 3950
Text GLabel 6450 3850 2    50   Input ~ 0
SPI_0_MISO
Text GLabel 6450 3650 2    50   Input ~ 0
UART_0_RXD
Text GLabel 6450 3750 2    50   Input ~ 0
SPI_0_CLK
Text GLabel 6450 3550 2    50   Input ~ 0
UART_0_TXD
Text GLabel 6450 3450 2    50   Input ~ 0
I2C_0_SDA
Wire Wire Line
	6450 3050 6475 3050
Connection ~ 6475 3050
Wire Wire Line
	6450 2850 6550 2850
Wire Wire Line
	6450 2950 6475 2950
Wire Wire Line
	6475 2950 6475 3050
Wire Wire Line
	6475 3150 6475 3250
Wire Wire Line
	6450 3150 6475 3150
Wire Wire Line
	6475 3050 6550 3050
Wire Wire Line
	6475 3250 6550 3250
Wire Wire Line
	6450 3250 6475 3250
Wire Wire Line
	6450 3350 6550 3350
Connection ~ 6475 3250
Text GLabel 5950 4050 0    50   Input ~ 0
CAN_H
Text GLabel 5950 4350 0    50   Input ~ 0
GPIO1
Text GLabel 5950 4450 0    50   Input ~ 0
GPIO3
Text GLabel 5950 4550 0    50   Input ~ 0
GPIO5_ADC1
Text GLabel 5950 4150 0    50   Input ~ 0
CAN_L
Text GLabel 5950 4750 0    50   Input ~ 0
PWM2
Text GLabel 5950 4650 0    50   Input ~ 0
GPIO7_ADC4
Wire Wire Line
	6450 5150 6550 5150
Wire Wire Line
	5850 5150 5950 5150
NoConn ~ 5950 4850
Text GLabel 5950 5050 0    50   Input ~ 0
PPS
Text GLabel 5950 4950 0    50   Input ~ 0
MCLK
Text GLabel 6450 4850 2    50   Input ~ 0
PWM3
Text GLabel 6450 4950 2    50   Input ~ 0
DMA_TRIG
Text GLabel 6450 5050 2    50   Input ~ 0
EXT_RST
Wire Wire Line
	6450 5450 6550 5450
Text GLabel 6450 5650 2    50   Input ~ 0
I2C_ISOL_SCL
Wire Wire Line
	6450 5750 6550 5750
Text GLabel 6450 5550 2    50   Input ~ 0
I2C_ISOL_SDA
$Comp
L spacemanic_power:GND_ISOL #PWR8
U 1 1 5E4EC71E
P 6550 5750
F 0 "#PWR8" H 6725 5600 50  0001 C CNN
F 1 "GND_ISOL" V 6546 5622 50  0000 R CNN
F 2 "" H 6550 5750 50  0001 C CNN
F 3 "" H 6550 5750 50  0001 C CNN
	1    6550 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5825 5450 5950 5450
Wire Wire Line
	5850 5750 5950 5750
Text GLabel 5950 5650 0    50   Input ~ 0
I2C_ISOL_SCL
Text GLabel 5950 5550 0    50   Input ~ 0
I2C_ISOL_SDA
$Comp
L spacemanic_power:GND_ISOL #PWR7
U 1 1 5E4EC762
P 5850 5750
F 0 "#PWR7" H 6025 5600 50  0001 C CNN
F 1 "GND_ISOL" V 5845 5622 50  0000 R CNN
F 2 "" H 5850 5750 50  0001 C CNN
F 3 "" H 5850 5750 50  0001 C CNN
	1    5850 5750
	0    1    1    0   
$EndComp
$Comp
L power:+BATT #PWR0105
U 1 1 5E4EC3B2
P 6550 2850
F 0 "#PWR0105" H 6550 2700 50  0001 C CNN
F 1 "+BATT" V 6565 2978 50  0000 L CNN
F 2 "" H 6550 2850 50  0001 C CNN
F 3 "" H 6550 2850 50  0001 C CNN
	1    6550 2850
	0    1    1    0   
$EndComp
$Comp
L power:+BATT #PWR0106
U 1 1 5E4EC3D2
P 5850 2850
F 0 "#PWR0106" H 5850 2700 50  0001 C CNN
F 1 "+BATT" V 5865 2978 50  0000 L CNN
F 2 "" H 5850 2850 50  0001 C CNN
F 3 "" H 5850 2850 50  0001 C CNN
	1    5850 2850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5E4EC81A
P 6550 3350
F 0 "#PWR0116" H 6550 3100 50  0001 C CNN
F 1 "GND" V 6555 3222 50  0000 R CNN
F 2 "" H 6550 3350 50  0001 C CNN
F 3 "" H 6550 3350 50  0001 C CNN
	1    6550 3350
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+3.3V_IN #PWR4
U 1 1 5E4EC419
P 6550 3250
F 0 "#PWR4" H 6550 3175 50  0001 C CNN
F 1 "+3.3V_IN" V 6536 3378 50  0000 L CNN
F 2 "" H 6550 3250 50  0001 C CNN
F 3 "" H 6550 3250 50  0001 C CNN
	1    6550 3250
	0    1    1    0   
$EndComp
$Comp
L spacemanic_power:+3.3V_IN #PWR3
U 1 1 5E4EC7C9
P 5850 3250
F 0 "#PWR3" H 5850 3175 50  0001 C CNN
F 1 "+3.3V_IN" V 5836 3378 50  0000 L CNN
F 2 "" H 5850 3250 50  0001 C CNN
F 3 "" H 5850 3250 50  0001 C CNN
	1    5850 3250
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+5V_IN #PWR2
U 1 1 5E4EC434
P 6550 3050
F 0 "#PWR2" H 6550 2975 50  0001 C CNN
F 1 "+5V_IN" V 6536 3178 50  0000 L CNN
F 2 "" H 6550 3050 50  0001 C CNN
F 3 "" H 6550 3050 50  0001 C CNN
	1    6550 3050
	0    1    1    0   
$EndComp
$Comp
L spacemanic_power:+5V_IN #PWR1
U 1 1 5E4EC3ED
P 5850 3050
F 0 "#PWR1" H 5850 2975 50  0001 C CNN
F 1 "+5V_IN" V 5836 3178 50  0000 L CNN
F 2 "" H 5850 3050 50  0001 C CNN
F 3 "" H 5850 3050 50  0001 C CNN
	1    5850 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 4250 5950 4250
$Comp
L power:GND #PWR0104
U 1 1 5E4EC796
P 5850 5150
F 0 "#PWR0104" H 5850 4900 50  0001 C CNN
F 1 "GND" V 5855 5022 50  0000 R CNN
F 2 "" H 5850 5150 50  0001 C CNN
F 3 "" H 5850 5150 50  0001 C CNN
	1    5850 5150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E4EC84A
P 5850 3950
F 0 "#PWR0102" H 5850 3700 50  0001 C CNN
F 1 "GND" V 5855 3822 50  0000 R CNN
F 2 "" H 5850 3950 50  0001 C CNN
F 3 "" H 5850 3950 50  0001 C CNN
	1    5850 3950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E4EC833
P 5850 4250
F 0 "#PWR0101" H 5850 4000 50  0001 C CNN
F 1 "GND" V 5855 4122 50  0000 R CNN
F 2 "" H 5850 4250 50  0001 C CNN
F 3 "" H 5850 4250 50  0001 C CNN
	1    5850 4250
	0    1    1    0   
$EndComp
$Comp
L MLAB_HEADER:HEADER_2x30 J5
U 1 1 5E4EC44F
P 6200 4300
F 0 "J5" H 6200 6104 60  0000 C CNN
F 1 "HEADER_2x30" H 6200 5997 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x30_Pitch2.54mm" H 6200 5890 60  0001 C CNN
F 3 "" H 6200 5750 60  0000 C CNN
	1    6200 4300
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5E4EC803
P 6550 3950
F 0 "#PWR0113" H 6550 3700 50  0001 C CNN
F 1 "GND" V 6555 3822 50  0000 R CNN
F 2 "" H 6550 3950 50  0001 C CNN
F 3 "" H 6550 3950 50  0001 C CNN
	1    6550 3950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5E4EC77C
P 6550 5150
F 0 "#PWR0114" H 6550 4900 50  0001 C CNN
F 1 "GND" V 6555 5022 50  0000 R CNN
F 2 "" H 6550 5150 50  0001 C CNN
F 3 "" H 6550 5150 50  0001 C CNN
	1    6550 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5E4EC7EC
P 6550 4250
F 0 "#PWR0115" H 6550 4000 50  0001 C CNN
F 1 "GND" V 6555 4122 50  0000 R CNN
F 2 "" H 6550 4250 50  0001 C CNN
F 3 "" H 6550 4250 50  0001 C CNN
	1    6550 4250
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+3.3V_ISOL #PWR6
U 1 1 5E4EC703
P 6550 5450
F 0 "#PWR6" H 6525 5375 50  0001 C CNN
F 1 "+3.3V_ISOL" V 6535 5578 50  0000 L CNN
F 2 "" H 6775 5825 50  0001 C CNN
F 3 "" H 6775 5825 50  0001 C CNN
	1    6550 5450
	0    1    1    0   
$EndComp
$Comp
L spacemanic_power:+3.3V_ISOL #PWR5
U 1 1 5E4EC747
P 5825 5450
F 0 "#PWR5" H 5800 5375 50  0001 C CNN
F 1 "+3.3V_ISOL" V 5811 5578 50  0000 L CNN
F 2 "" H 6050 5825 50  0001 C CNN
F 3 "" H 6050 5825 50  0001 C CNN
	1    5825 5450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 3950 8050 3950
Text GLabel 8050 3850 0    50   Input ~ 0
SPI_0_MOSI
Text GLabel 8050 3750 0    50   Input ~ 0
SPI_0_CS
Wire Wire Line
	8575 3050 8650 3050
Text Notes 8600 2725 2    50   ~ 0
Module connector
Wire Wire Line
	8550 2850 8650 2850
Wire Wire Line
	8550 3050 8575 3050
Connection ~ 8575 3050
Wire Wire Line
	8025 2950 8025 3050
Wire Wire Line
	8050 2950 8025 2950
Wire Wire Line
	8575 2950 8575 3050
Wire Wire Line
	8550 2950 8575 2950
Wire Wire Line
	8025 3050 8050 3050
Connection ~ 8025 3050
Wire Wire Line
	7950 3050 8025 3050
Wire Wire Line
	7950 2850 8050 2850
Connection ~ 8025 3250
Wire Wire Line
	8050 3150 8025 3150
Wire Wire Line
	8025 3250 8050 3250
Wire Wire Line
	7950 3250 8025 3250
Wire Wire Line
	8025 3150 8025 3250
Wire Wire Line
	7950 3350 8050 3350
Text GLabel 8050 3650 0    50   Input ~ 0
RS485_A
Text GLabel 8050 3550 0    50   Input ~ 0
RS485_B
Text GLabel 8050 3450 0    50   Input ~ 0
I2C_0_SCL
Text GLabel 8050 4550 0    50   Input ~ 0
GPIO5_ADC1
Text GLabel 8050 4450 0    50   Input ~ 0
GPIO3
Text GLabel 8050 4350 0    50   Input ~ 0
GPIO1
Text GLabel 8050 4150 0    50   Input ~ 0
CAN_L
Wire Wire Line
	7950 4250 8050 4250
Text GLabel 8050 4050 0    50   Input ~ 0
CAN_H
Text GLabel 8050 4650 0    50   Input ~ 0
GPIO7_ADC4
Text GLabel 8050 4750 0    50   Input ~ 0
PWM2
Text GLabel 8050 4950 0    50   Input ~ 0
MCLK
Text GLabel 8050 5050 0    50   Input ~ 0
PPS
NoConn ~ 8050 4850
Wire Wire Line
	8550 5150 8650 5150
Text GLabel 8550 5050 2    50   Input ~ 0
EXT_RST
Text GLabel 8550 4950 2    50   Input ~ 0
DMA_TRIG
Text GLabel 8550 4850 2    50   Input ~ 0
PWM3
Wire Wire Line
	7950 5150 8050 5150
NoConn ~ 8050 5250
NoConn ~ 8050 5350
Wire Wire Line
	7925 5450 8050 5450
Wire Wire Line
	7950 5750 8050 5750
Text GLabel 8050 5650 0    50   Input ~ 0
I2C_ISOL_SCL
Text GLabel 8050 5550 0    50   Input ~ 0
I2C_ISOL_SDA
$Comp
L spacemanic_power:GND_ISOL #PWR12
U 1 1 5E6441EB
P 7950 5750
F 0 "#PWR12" H 8125 5600 50  0001 C CNN
F 1 "GND_ISOL" V 7945 5622 50  0000 R CNN
F 2 "" H 7950 5750 50  0001 C CNN
F 3 "" H 7950 5750 50  0001 C CNN
	1    7950 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	8550 3150 8575 3150
Connection ~ 8575 3250
Wire Wire Line
	8575 3250 8650 3250
Wire Wire Line
	8550 3250 8575 3250
Wire Wire Line
	8575 3150 8575 3250
$Comp
L power:GND #PWR0125
U 1 1 5E710BB7
P 9225 2475
F 0 "#PWR0125" H 9225 2225 50  0001 C CNN
F 1 "GND" V 9230 2347 50  0000 R CNN
F 2 "" H 9225 2475 50  0001 C CNN
F 3 "" H 9225 2475 50  0001 C CNN
	1    9225 2475
	0    1    1    0   
$EndComp
Text GLabel 8550 3450 2    50   Input ~ 0
I2C_0_SDA
Text GLabel 8550 3550 2    50   Input ~ 0
UART_0_TXD
Text GLabel 8550 3650 2    50   Input ~ 0
UART_0_RXD
Wire Wire Line
	8550 3350 8650 3350
$Comp
L power:GND #PWR0110
U 1 1 5E5D3C45
P 8650 3950
F 0 "#PWR0110" H 8650 3700 50  0001 C CNN
F 1 "GND" V 8655 3822 50  0000 R CNN
F 2 "" H 8650 3950 50  0001 C CNN
F 3 "" H 8650 3950 50  0001 C CNN
	1    8650 3950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E588945
P 8650 3350
F 0 "#PWR0107" H 8650 3100 50  0001 C CNN
F 1 "GND" V 8655 3222 50  0000 R CNN
F 2 "" H 8650 3350 50  0001 C CNN
F 3 "" H 8650 3350 50  0001 C CNN
	1    8650 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10225 3350 10225 3250
Wire Wire Line
	10000 3350 10225 3350
Connection ~ 10000 3350
Wire Wire Line
	10000 3250 10000 3350
Wire Wire Line
	9750 3350 10000 3350
Wire Wire Line
	9500 3250 9500 3350
Connection ~ 9750 3350
Wire Wire Line
	9500 3350 9750 3350
Wire Wire Line
	9750 3250 9750 3350
Wire Wire Line
	9500 2475 9500 2400
Wire Wire Line
	9425 2475 9500 2475
$Comp
L device:L_Small L1
U 1 1 5E702FEF
P 9325 2475
F 0 "L1" V 9418 2475 50  0000 C CNN
F 1 "L_Small" H 9373 2430 50  0001 L CNN
F 2 "Inductors_SMD:L_1210_HandSoldering" H 9325 2475 50  0001 C CNN
F 3 "~" H 9325 2475 50  0001 C CNN
	1    9325 2475
	0    -1   -1   0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M2
U 1 1 5D096AD3
P 9750 2350
F 0 "M2" V 9650 2350 60  0000 C CNN
F 1 "HOLE" H 9769 2458 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 9769 2458 60  0001 C CNN
F 3 "" H 9750 2350 60  0000 C CNN
	1    9750 2350
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M4
U 1 1 5D097003
P 10225 2350
F 0 "M4" V 10125 2350 60  0000 C CNN
F 1 "HOLE" H 10244 2458 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 10244 2458 60  0001 C CNN
F 3 "" H 10225 2350 60  0000 C CNN
	1    10225 2350
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M3
U 1 1 5D096D66
P 10000 2350
F 0 "M3" V 9900 2350 60  0000 C CNN
F 1 "HOLE" H 10019 2458 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 10019 2458 60  0001 C CNN
F 3 "" H 10000 2350 60  0000 C CNN
	1    10000 2350
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M1
U 1 1 5D096865
P 9500 2350
F 0 "M1" V 9400 2350 60  0000 C CNN
F 1 "HOLE" V 9400 2350 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 9519 2458 60  0001 C CNN
F 3 "" H 9500 2350 60  0000 C CNN
	1    9500 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 3350 9750 3450
$Comp
L MLAB_MECHANICAL:HOLE M6
U 1 1 5E43C207
P 9750 3200
F 0 "M6" V 9650 3200 60  0000 C CNN
F 1 "HOLE" V 9650 3200 60  0001 C CNN
F 2 "Spacemanic_mechanical:Spacer_hole_4.4mm" H 9769 3308 60  0001 C CNN
F 3 "" H 9750 3200 60  0000 C CNN
	1    9750 3200
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M8
U 1 1 5E43C1D9
P 10225 3200
F 0 "M8" V 10125 3200 60  0000 C CNN
F 1 "HOLE" V 10125 3225 60  0001 C CNN
F 2 "Spacemanic_mechanical:Spacer_hole_4.4mm" H 10244 3308 60  0001 C CNN
F 3 "" H 10225 3200 60  0000 C CNN
	1    10225 3200
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M7
U 1 1 5E43C21E
P 10000 3200
F 0 "M7" V 9900 3200 60  0000 C CNN
F 1 "HOLE" V 9900 3200 60  0001 C CNN
F 2 "Spacemanic_mechanical:Spacer_hole_4.4mm" H 10019 3308 60  0001 C CNN
F 3 "" H 10000 3200 60  0000 C CNN
	1    10000 3200
	0    1    1    0   
$EndComp
$Comp
L MLAB_MECHANICAL:HOLE M5
U 1 1 5E43C1F0
P 9500 3200
F 0 "M5" V 9400 3200 60  0000 C CNN
F 1 "HOLE" V 9400 3200 60  0001 C CNN
F 2 "Spacemanic_mechanical:Spacer_hole_4.4mm" H 9519 3308 60  0001 C CNN
F 3 "" H 9500 3200 60  0000 C CNN
	1    9500 3200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5E512676
P 9750 3450
F 0 "#PWR0121" H 9750 3200 50  0001 C CNN
F 1 "GND" V 9755 3322 50  0000 R CNN
F 2 "" H 9750 3450 50  0001 C CNN
F 3 "" H 9750 3450 50  0001 C CNN
	1    9750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 5450 8650 5450
NoConn ~ 8550 5350
NoConn ~ 8550 5250
Wire Wire Line
	8550 5750 8650 5750
Text GLabel 8550 5650 2    50   Input ~ 0
I2C_ISOL_SCL
Text GLabel 8550 5550 2    50   Input ~ 0
I2C_ISOL_SDA
$Comp
L spacemanic_power:GND_ISOL #PWR16
U 1 1 5E6435EE
P 8650 5750
F 0 "#PWR16" H 8825 5600 50  0001 C CNN
F 1 "GND_ISOL" V 8646 5622 50  0000 R CNN
F 2 "" H 8650 5750 50  0001 C CNN
F 3 "" H 8650 5750 50  0001 C CNN
	1    8650 5750
	0    -1   -1   0   
$EndComp
Text GLabel 8550 4350 2    50   Input ~ 0
GPIO0
Wire Wire Line
	8550 4250 8650 4250
Wire Wire Line
	8550 3950 8650 3950
Text GLabel 8550 3750 2    50   Input ~ 0
SPI_0_CLK
Text GLabel 8550 3850 2    50   Input ~ 0
SPI_0_MISO
Text GLabel 8550 4750 2    50   Input ~ 0
PWM1
Text GLabel 8550 4450 2    50   Input ~ 0
GPIO2
Text GLabel 8550 4650 2    50   Input ~ 0
GPIO6_ADC2
Text GLabel 8550 4550 2    50   Input ~ 0
GPIOO4_ADC0
$Comp
L power:+BATT #PWR0111
U 1 1 5E4E3F25
P 8650 2850
F 0 "#PWR0111" H 8650 2700 50  0001 C CNN
F 1 "+BATT" V 8665 2978 50  0000 L CNN
F 2 "" H 8650 2850 50  0001 C CNN
F 3 "" H 8650 2850 50  0001 C CNN
	1    8650 2850
	0    1    1    0   
$EndComp
$Comp
L power:+BATT #PWR0117
U 1 1 5E4DD784
P 7950 2850
F 0 "#PWR0117" H 7950 2700 50  0001 C CNN
F 1 "+BATT" V 7965 2978 50  0000 L CNN
F 2 "" H 7950 2850 50  0001 C CNN
F 3 "" H 7950 2850 50  0001 C CNN
	1    7950 2850
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+3.3V_IN #PWR14
U 1 1 5E574F89
P 8650 3250
F 0 "#PWR14" H 8650 3175 50  0001 C CNN
F 1 "+3.3V_IN" V 8636 3378 50  0000 L CNN
F 2 "" H 8650 3250 50  0001 C CNN
F 3 "" H 8650 3250 50  0001 C CNN
	1    8650 3250
	0    1    1    0   
$EndComp
$Comp
L spacemanic_power:+3.3V_IN #PWR11
U 1 1 5E574656
P 7950 3250
F 0 "#PWR11" H 7950 3175 50  0001 C CNN
F 1 "+3.3V_IN" V 7936 3378 50  0000 L CNN
F 2 "" H 7950 3250 50  0001 C CNN
F 3 "" H 7950 3250 50  0001 C CNN
	1    7950 3250
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+5V_IN #PWR10
U 1 1 5E55D099
P 7950 3050
F 0 "#PWR10" H 7950 2975 50  0001 C CNN
F 1 "+5V_IN" V 7936 3178 50  0000 L CNN
F 2 "" H 7950 3050 50  0001 C CNN
F 3 "" H 7950 3050 50  0001 C CNN
	1    7950 3050
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+5V_IN #PWR13
U 1 1 5E55DC28
P 8650 3050
F 0 "#PWR13" H 8650 2975 50  0001 C CNN
F 1 "+5V_IN" V 8636 3178 50  0000 L CNN
F 2 "" H 8650 3050 50  0001 C CNN
F 3 "" H 8650 3050 50  0001 C CNN
	1    8650 3050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E5D3C2E
P 7950 3950
F 0 "#PWR0108" H 7950 3700 50  0001 C CNN
F 1 "GND" V 7955 3822 50  0000 R CNN
F 2 "" H 7950 3950 50  0001 C CNN
F 3 "" H 7950 3950 50  0001 C CNN
	1    7950 3950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5E5D6A55
P 8650 4250
F 0 "#PWR0112" H 8650 4000 50  0001 C CNN
F 1 "GND" V 8655 4122 50  0000 R CNN
F 2 "" H 8650 4250 50  0001 C CNN
F 3 "" H 8650 4250 50  0001 C CNN
	1    8650 4250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5E634BCA
P 8650 5150
F 0 "#PWR0120" H 8650 4900 50  0001 C CNN
F 1 "GND" V 8655 5022 50  0000 R CNN
F 2 "" H 8650 5150 50  0001 C CNN
F 3 "" H 8650 5150 50  0001 C CNN
	1    8650 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5E634BB3
P 7950 5150
F 0 "#PWR0119" H 7950 4900 50  0001 C CNN
F 1 "GND" V 7955 5022 50  0000 R CNN
F 2 "" H 7950 5150 50  0001 C CNN
F 3 "" H 7950 5150 50  0001 C CNN
	1    7950 5150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E5D6A3E
P 7950 4250
F 0 "#PWR0109" H 7950 4000 50  0001 C CNN
F 1 "GND" V 7955 4122 50  0000 R CNN
F 2 "" H 7950 4250 50  0001 C CNN
F 3 "" H 7950 4250 50  0001 C CNN
	1    7950 4250
	0    1    1    0   
$EndComp
$Comp
L MLAB_HEADER:HEADER_2x30 J6
U 1 1 5E463BC1
P 8300 4300
F 0 "J6" H 8300 6104 60  0000 C CNN
F 1 "HEADER_2x30" H 8300 5997 60  0000 C CNN
F 2 "Spacemanic_connector:Samtec_TLH-030" H 8300 5890 60  0001 C CNN
F 3 "" H 8300 5750 60  0000 C CNN
	1    8300 4300
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5E585E13
P 7950 3350
F 0 "#PWR0118" H 7950 3100 50  0001 C CNN
F 1 "GND" V 7955 3222 50  0000 R CNN
F 2 "" H 7950 3350 50  0001 C CNN
F 3 "" H 7950 3350 50  0001 C CNN
	1    7950 3350
	0    1    1    0   
$EndComp
$Comp
L spacemanic_power:+3.3V_ISOL #PWR9
U 1 1 5E63DEF5
P 7925 5450
F 0 "#PWR9" H 7900 5375 50  0001 C CNN
F 1 "+3.3V_ISOL" V 7911 5578 50  0000 L CNN
F 2 "" H 8150 5825 50  0001 C CNN
F 3 "" H 8150 5825 50  0001 C CNN
	1    7925 5450
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+3.3V_ISOL #PWR15
U 1 1 5E63E9D5
P 8650 5450
F 0 "#PWR15" H 8625 5375 50  0001 C CNN
F 1 "+3.3V_ISOL" V 8635 5578 50  0000 L CNN
F 2 "" H 8875 5825 50  0001 C CNN
F 3 "" H 8875 5825 50  0001 C CNN
	1    8650 5450
	0    1    1    0   
$EndComp
NoConn ~ 8550 4050
NoConn ~ 8550 4150
$EndSCHEMATC

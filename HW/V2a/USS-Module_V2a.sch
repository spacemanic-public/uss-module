EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "USS-Module_V2a"
Date "2022-03-22"
Rev "V2a"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "sch"
$EndDescr
Wire Wire Line
	3825 1750 3875 1750
Wire Wire Line
	3875 1750 3875 1850
Text GLabel 5950 2250 0    50   Input ~ 0
RS485_B
Text GLabel 5950 2550 0    50   Input ~ 0
SPI_0_MOSI
Text GLabel 5950 2450 0    50   Input ~ 0
SPI_0_CS
Text GLabel 5950 2350 0    50   Input ~ 0
RS485_A
Wire Wire Line
	5850 2650 5950 2650
Text GLabel 5950 2150 0    50   Input ~ 0
I2C_0_SCL
Wire Wire Line
	5925 1850 5925 1950
Connection ~ 5925 1950
Wire Wire Line
	5925 1950 5950 1950
Wire Wire Line
	5850 1950 5925 1950
Wire Wire Line
	5850 2050 5950 2050
$Comp
L power:GND #PWR0103
U 1 1 5E4EC7B2
P 5850 2050
F 0 "#PWR0103" H 5850 1800 50  0001 C CNN
F 1 "GND" V 5855 1922 50  0000 R CNN
F 2 "" H 5850 2050 50  0001 C CNN
F 3 "" H 5850 2050 50  0001 C CNN
	1    5850 2050
	0    1    1    0   
$EndComp
Connection ~ 5925 1750
Wire Wire Line
	5950 1850 5925 1850
Wire Wire Line
	5925 1750 5950 1750
Wire Wire Line
	5925 1650 5925 1750
Wire Wire Line
	5950 1650 5925 1650
Wire Wire Line
	5850 1550 5950 1550
Wire Wire Line
	5850 1750 5925 1750
Wire Wire Line
	4475 1425 4525 1425
Wire Wire Line
	4525 1525 4475 1525
Wire Wire Line
	4525 1425 4525 1525
Wire Wire Line
	4525 1325 4525 1425
Connection ~ 4525 1425
Wire Wire Line
	3875 1950 3875 2050
Wire Wire Line
	3875 1850 3875 1950
Wire Wire Line
	3825 1950 3875 1950
Connection ~ 3875 1950
Wire Wire Line
	3825 1850 3875 1850
Wire Wire Line
	4200 1750 4200 2050
Connection ~ 3875 1850
Text GLabel 4150 1750 0    50   Input ~ 0
2.31
Wire Wire Line
	4150 1750 4200 1750
Text GLabel 4475 1525 0    50   Input ~ 0
2.28
Text GLabel 4475 1425 0    50   Input ~ 0
2.27
$Comp
L Device:Jumper_NC_Small JP3
U 1 1 5E6922C9
P 4525 1225
F 0 "JP3" V 4525 1425 50  0000 R CNN
F 1 "Jumper_NC_Small" H 4525 1345 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4525 1225 50  0001 C CNN
F 3 "~" H 4525 1225 50  0001 C CNN
F 4 "header 2x01 p2.54mm" V 4525 1225 50  0001 C CNN "MFPN"
	1    4525 1225
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Small JP2
U 1 1 5E68D70D
P 4200 1225
F 0 "JP2" V 4200 1425 50  0000 R CNN
F 1 "Jumper_NC_Small" H 4200 1345 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4200 1225 50  0001 C CNN
F 3 "~" H 4200 1225 50  0001 C CNN
F 4 "header 2x01 p2.54mm" V 4200 1225 50  0001 C CNN "MFPN"
	1    4200 1225
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3875 1525 3825 1525
Wire Wire Line
	3825 1425 3875 1425
Wire Wire Line
	3875 1425 3875 1525
Text GLabel 4150 1425 0    50   Input ~ 0
2.25
Wire Wire Line
	4200 1425 4200 1525
Wire Wire Line
	4150 1425 4200 1425
Wire Wire Line
	3875 1325 3875 1425
Wire Wire Line
	4200 1325 4200 1425
Connection ~ 4200 1425
Connection ~ 3875 1425
Text GLabel 4150 1525 0    50   Input ~ 0
2.26
Wire Wire Line
	4200 1525 4150 1525
$Comp
L Device:Jumper_NC_Small JP1
U 1 1 5E668409
P 3875 1225
F 0 "JP1" V 3875 1425 50  0000 R CNN
F 1 "Jumper_NC_Small" H 3875 1345 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3875 1225 50  0001 C CNN
F 3 "~" H 3875 1225 50  0001 C CNN
F 4 "header 2x01 p2.54mm" V 3875 1225 50  0001 C CNN "MFPN"
	1    3875 1225
	0    -1   -1   0   
$EndComp
Text GLabel 6450 3050 2    50   Input ~ 0
GPIO0
Text GLabel 6450 2850 2    50   Input ~ 0
CAN_L
Wire Wire Line
	6450 2950 6550 2950
Text GLabel 6450 3350 2    50   Input ~ 0
GPIO6_ADC2
Text GLabel 6450 3450 2    50   Input ~ 0
PWM1
Text GLabel 6450 3150 2    50   Input ~ 0
GPIO2
Text GLabel 6450 3250 2    50   Input ~ 0
GPIOO4_ADC0
Text GLabel 6450 2750 2    50   Input ~ 0
CAN_H
Wire Wire Line
	6450 2650 6550 2650
Text GLabel 6450 2550 2    50   Input ~ 0
SPI_0_MISO
Text GLabel 6450 2350 2    50   Input ~ 0
UART_0_RXD
Text GLabel 6450 2450 2    50   Input ~ 0
SPI_0_CLK
Text GLabel 6450 2250 2    50   Input ~ 0
UART_0_TXD
Text GLabel 6450 2150 2    50   Input ~ 0
I2C_0_SDA
Wire Wire Line
	6450 1750 6475 1750
Connection ~ 6475 1750
Wire Wire Line
	6450 1550 6550 1550
Wire Wire Line
	6450 1650 6475 1650
Wire Wire Line
	6475 1650 6475 1750
Wire Wire Line
	6475 1850 6475 1950
Wire Wire Line
	6450 1850 6475 1850
Wire Wire Line
	6475 1750 6550 1750
Wire Wire Line
	6475 1950 6550 1950
Wire Wire Line
	6450 1950 6475 1950
Wire Wire Line
	6450 2050 6550 2050
Connection ~ 6475 1950
Text GLabel 5950 2750 0    50   Input ~ 0
CAN_H
Text GLabel 5950 3050 0    50   Input ~ 0
GPIO1
Text GLabel 5950 3150 0    50   Input ~ 0
GPIO3
Text GLabel 5950 3250 0    50   Input ~ 0
GPIO5_ADC1
Text GLabel 5950 2850 0    50   Input ~ 0
CAN_L
Text GLabel 5950 3450 0    50   Input ~ 0
PWM2
Text GLabel 5950 3350 0    50   Input ~ 0
GPIO7_ADC3
Wire Wire Line
	6450 3850 6550 3850
Wire Wire Line
	5850 3850 5950 3850
NoConn ~ 5950 3550
Text GLabel 5950 3750 0    50   Input ~ 0
PPS
Text GLabel 5950 3650 0    50   Input ~ 0
MCLK
Text GLabel 6450 3550 2    50   Input ~ 0
PWM3
Text GLabel 6450 3650 2    50   Input ~ 0
DMA_TRIG
Text GLabel 6450 3750 2    50   Input ~ 0
EXT_RST
Wire Wire Line
	6450 4150 6550 4150
Text GLabel 6450 4350 2    50   Input ~ 0
I2C_ISOL_SCL
Wire Wire Line
	6450 4450 6550 4450
Text GLabel 6450 4250 2    50   Input ~ 0
I2C_ISOL_SDA
$Comp
L spacemanic_power:GND_ISOL #PWR8
U 1 1 5E4EC71E
P 6550 4450
F 0 "#PWR8" H 6725 4300 50  0001 C CNN
F 1 "GND_ISOL" V 6546 4322 50  0000 R CNN
F 2 "" H 6550 4450 50  0001 C CNN
F 3 "" H 6550 4450 50  0001 C CNN
	1    6550 4450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5825 4150 5950 4150
Wire Wire Line
	5850 4450 5950 4450
Text GLabel 5950 4350 0    50   Input ~ 0
I2C_ISOL_SCL
Text GLabel 5950 4250 0    50   Input ~ 0
I2C_ISOL_SDA
$Comp
L spacemanic_power:GND_ISOL #PWR7
U 1 1 5E4EC762
P 5850 4450
F 0 "#PWR7" H 6025 4300 50  0001 C CNN
F 1 "GND_ISOL" V 5845 4322 50  0000 R CNN
F 2 "" H 5850 4450 50  0001 C CNN
F 3 "" H 5850 4450 50  0001 C CNN
	1    5850 4450
	0    1    1    0   
$EndComp
$Comp
L power:+BATT #PWR0105
U 1 1 5E4EC3B2
P 6550 1550
F 0 "#PWR0105" H 6550 1400 50  0001 C CNN
F 1 "+BATT" V 6565 1678 50  0000 L CNN
F 2 "" H 6550 1550 50  0001 C CNN
F 3 "" H 6550 1550 50  0001 C CNN
	1    6550 1550
	0    1    1    0   
$EndComp
$Comp
L power:+BATT #PWR0106
U 1 1 5E4EC3D2
P 5850 1550
F 0 "#PWR0106" H 5850 1400 50  0001 C CNN
F 1 "+BATT" V 5865 1678 50  0000 L CNN
F 2 "" H 5850 1550 50  0001 C CNN
F 3 "" H 5850 1550 50  0001 C CNN
	1    5850 1550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5E4EC81A
P 6550 2050
F 0 "#PWR0116" H 6550 1800 50  0001 C CNN
F 1 "GND" V 6555 1922 50  0000 R CNN
F 2 "" H 6550 2050 50  0001 C CNN
F 3 "" H 6550 2050 50  0001 C CNN
	1    6550 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 2950 5950 2950
$Comp
L power:GND #PWR0104
U 1 1 5E4EC796
P 5850 3850
F 0 "#PWR0104" H 5850 3600 50  0001 C CNN
F 1 "GND" V 5855 3722 50  0000 R CNN
F 2 "" H 5850 3850 50  0001 C CNN
F 3 "" H 5850 3850 50  0001 C CNN
	1    5850 3850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E4EC84A
P 5850 2650
F 0 "#PWR0102" H 5850 2400 50  0001 C CNN
F 1 "GND" V 5855 2522 50  0000 R CNN
F 2 "" H 5850 2650 50  0001 C CNN
F 3 "" H 5850 2650 50  0001 C CNN
	1    5850 2650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E4EC833
P 5850 2950
F 0 "#PWR0101" H 5850 2700 50  0001 C CNN
F 1 "GND" V 5855 2822 50  0000 R CNN
F 2 "" H 5850 2950 50  0001 C CNN
F 3 "" H 5850 2950 50  0001 C CNN
	1    5850 2950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5E4EC803
P 6550 2650
F 0 "#PWR0113" H 6550 2400 50  0001 C CNN
F 1 "GND" V 6555 2522 50  0000 R CNN
F 2 "" H 6550 2650 50  0001 C CNN
F 3 "" H 6550 2650 50  0001 C CNN
	1    6550 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5E4EC77C
P 6550 3850
F 0 "#PWR0114" H 6550 3600 50  0001 C CNN
F 1 "GND" V 6555 3722 50  0000 R CNN
F 2 "" H 6550 3850 50  0001 C CNN
F 3 "" H 6550 3850 50  0001 C CNN
	1    6550 3850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5E4EC7EC
P 6550 2950
F 0 "#PWR0115" H 6550 2700 50  0001 C CNN
F 1 "GND" V 6555 2822 50  0000 R CNN
F 2 "" H 6550 2950 50  0001 C CNN
F 3 "" H 6550 2950 50  0001 C CNN
	1    6550 2950
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+3.3V_ISOL #PWR6
U 1 1 5E4EC703
P 6550 4150
F 0 "#PWR6" H 6525 4075 50  0001 C CNN
F 1 "+3.3V_ISOL" V 6535 4278 50  0000 L CNN
F 2 "" H 6775 4525 50  0001 C CNN
F 3 "" H 6775 4525 50  0001 C CNN
	1    6550 4150
	0    1    1    0   
$EndComp
$Comp
L spacemanic_power:+3.3V_ISOL #PWR5
U 1 1 5E4EC747
P 5825 4150
F 0 "#PWR5" H 5800 4075 50  0001 C CNN
F 1 "+3.3V_ISOL" V 5811 4278 50  0000 L CNN
F 2 "" H 6050 4525 50  0001 C CNN
F 3 "" H 6050 4525 50  0001 C CNN
	1    5825 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7750 2650 7850 2650
Text GLabel 7850 2550 0    50   Input ~ 0
SPI_0_MOSI
Text GLabel 7850 2450 0    50   Input ~ 0
SPI_0_CS
Wire Wire Line
	8375 1750 8450 1750
Wire Wire Line
	8350 1550 8450 1550
Wire Wire Line
	8350 1750 8375 1750
Connection ~ 8375 1750
Wire Wire Line
	7825 1650 7825 1750
Wire Wire Line
	7850 1650 7825 1650
Wire Wire Line
	8375 1650 8375 1750
Wire Wire Line
	8350 1650 8375 1650
Wire Wire Line
	7825 1750 7850 1750
Connection ~ 7825 1750
Wire Wire Line
	7750 1750 7825 1750
Wire Wire Line
	7750 1550 7850 1550
Connection ~ 7825 1950
Wire Wire Line
	7850 1850 7825 1850
Wire Wire Line
	7825 1950 7850 1950
Wire Wire Line
	7750 1950 7825 1950
Wire Wire Line
	7825 1850 7825 1950
Wire Wire Line
	7750 2050 7850 2050
Text GLabel 7850 2350 0    50   Input ~ 0
RS485_A
Text GLabel 7850 2250 0    50   Input ~ 0
RS485_B
Text GLabel 7850 2150 0    50   Input ~ 0
I2C_0_SCL
Text GLabel 7850 3250 0    50   Input ~ 0
GPIO5_ADC1
Text GLabel 7850 3150 0    50   Input ~ 0
GPIO3
Text GLabel 7850 3050 0    50   Input ~ 0
GPIO1
Text GLabel 7850 2850 0    50   Input ~ 0
CAN_L
Wire Wire Line
	7750 2950 7850 2950
Text GLabel 7850 2750 0    50   Input ~ 0
CAN_H
Text GLabel 7850 3350 0    50   Input ~ 0
GPIO7_ADC3
Text GLabel 7850 3450 0    50   Input ~ 0
PWM2
Text GLabel 7850 3650 0    50   Input ~ 0
MCLK
Text GLabel 7850 3750 0    50   Input ~ 0
PPS
NoConn ~ 7850 3550
Wire Wire Line
	8350 3850 8450 3850
Text GLabel 8350 3750 2    50   Input ~ 0
EXT_RST
Text GLabel 8350 3650 2    50   Input ~ 0
DMA_TRIG
Text GLabel 8350 3550 2    50   Input ~ 0
PWM3
Wire Wire Line
	7750 3850 7850 3850
NoConn ~ 7850 3950
NoConn ~ 7850 4050
Wire Wire Line
	7725 4150 7850 4150
Wire Wire Line
	7750 4450 7850 4450
Text GLabel 7850 4350 0    50   Input ~ 0
I2C_ISOL_SCL
Text GLabel 7850 4250 0    50   Input ~ 0
I2C_ISOL_SDA
$Comp
L spacemanic_power:GND_ISOL #PWR12
U 1 1 5E6441EB
P 7750 4450
F 0 "#PWR12" H 7925 4300 50  0001 C CNN
F 1 "GND_ISOL" V 7745 4322 50  0000 R CNN
F 2 "" H 7750 4450 50  0001 C CNN
F 3 "" H 7750 4450 50  0001 C CNN
	1    7750 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	8350 1850 8375 1850
Connection ~ 8375 1950
Wire Wire Line
	8375 1950 8450 1950
Wire Wire Line
	8350 1950 8375 1950
Wire Wire Line
	8375 1850 8375 1950
$Comp
L power:GND #PWR0125
U 1 1 5E710BB7
P 9925 1000
F 0 "#PWR0125" H 9925 750 50  0001 C CNN
F 1 "GND" V 9930 872 50  0000 R CNN
F 2 "" H 9925 1000 50  0001 C CNN
F 3 "" H 9925 1000 50  0001 C CNN
	1    9925 1000
	0    1    1    0   
$EndComp
Text GLabel 8350 2150 2    50   Input ~ 0
I2C_0_SDA
Text GLabel 8350 2250 2    50   Input ~ 0
UART_0_TXD
Text GLabel 8350 2350 2    50   Input ~ 0
UART_0_RXD
Wire Wire Line
	8350 2050 8450 2050
$Comp
L power:GND #PWR0110
U 1 1 5E5D3C45
P 8450 2650
F 0 "#PWR0110" H 8450 2400 50  0001 C CNN
F 1 "GND" V 8455 2522 50  0000 R CNN
F 2 "" H 8450 2650 50  0001 C CNN
F 3 "" H 8450 2650 50  0001 C CNN
	1    8450 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E588945
P 8450 2050
F 0 "#PWR0107" H 8450 1800 50  0001 C CNN
F 1 "GND" V 8455 1922 50  0000 R CNN
F 2 "" H 8450 2050 50  0001 C CNN
F 3 "" H 8450 2050 50  0001 C CNN
	1    8450 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7500 1025 7500 975 
Wire Wire Line
	7250 1025 7500 1025
Connection ~ 7250 1025
Wire Wire Line
	7250 975  7250 1025
Wire Wire Line
	7000 1025 7250 1025
Wire Wire Line
	6750 975  6750 1025
Connection ~ 7000 1025
Wire Wire Line
	6750 1025 7000 1025
Wire Wire Line
	7000 975  7000 1025
Wire Wire Line
	10250 1000 10250 975 
Wire Wire Line
	10125 1000 10250 1000
$Comp
L Mechanical:MountingHole M2
U 1 1 5D096AD3
P 10500 825
F 0 "M2" V 10375 825 60  0000 C CNN
F 1 "HOLE" H 10519 933 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 10519 933 60  0001 C CNN
F 3 "" H 10500 825 60  0000 C CNN
	1    10500 825 
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole M4
U 1 1 5D097003
P 11000 825
F 0 "M4" V 10875 825 60  0000 C CNN
F 1 "HOLE" H 11019 933 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 11019 933 60  0001 C CNN
F 3 "" H 11000 825 60  0000 C CNN
	1    11000 825 
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole M3
U 1 1 5D096D66
P 10750 825
F 0 "M3" V 10625 825 60  0000 C CNN
F 1 "HOLE" H 10769 933 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 10769 933 60  0001 C CNN
F 3 "" H 10750 825 60  0000 C CNN
	1    10750 825 
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad M1
U 1 1 5D096865
P 10250 875
F 0 "M1" H 10250 1050 60  0000 C CNN
F 1 "HOLE" V 10150 875 60  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 10269 983 60  0001 C CNN
F 3 "" H 10250 875 60  0000 C CNN
	1    10250 875 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1025 7000 1075
$Comp
L Mechanical:MountingHole_Pad M6
U 1 1 5E43C207
P 7000 875
F 0 "M6" H 7000 1050 60  0000 C CNN
F 1 "HOLE" V 6900 875 60  0001 C CNN
F 2 "Spacemanic_mechanical:Spacer_hole_4.4mm" H 7019 983 60  0001 C CNN
F 3 "" H 7000 875 60  0000 C CNN
	1    7000 875 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M8
U 1 1 5E43C1D9
P 7500 875
F 0 "M8" H 7500 1050 60  0000 C CNN
F 1 "HOLE" V 7400 900 60  0001 C CNN
F 2 "Spacemanic_mechanical:Spacer_hole_4.4mm" H 7519 983 60  0001 C CNN
F 3 "" H 7500 875 60  0000 C CNN
	1    7500 875 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M7
U 1 1 5E43C21E
P 7250 875
F 0 "M7" H 7250 1050 60  0000 C CNN
F 1 "HOLE" V 7150 875 60  0001 C CNN
F 2 "Spacemanic_mechanical:Spacer_hole_4.4mm" H 7269 983 60  0001 C CNN
F 3 "" H 7250 875 60  0000 C CNN
	1    7250 875 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad M5
U 1 1 5E43C1F0
P 6750 875
F 0 "M5" H 6750 1050 60  0000 C CNN
F 1 "HOLE" V 6650 875 60  0001 C CNN
F 2 "Spacemanic_mechanical:Spacer_hole_4.4mm" H 6769 983 60  0001 C CNN
F 3 "" H 6750 875 60  0000 C CNN
	1    6750 875 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5E512676
P 7000 1075
F 0 "#PWR0121" H 7000 825 50  0001 C CNN
F 1 "GND" H 7075 925 50  0000 R CNN
F 2 "" H 7000 1075 50  0001 C CNN
F 3 "" H 7000 1075 50  0001 C CNN
	1    7000 1075
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 4150 8450 4150
NoConn ~ 8350 4050
NoConn ~ 8350 3950
Wire Wire Line
	8350 4450 8450 4450
Text GLabel 8350 4350 2    50   Input ~ 0
I2C_ISOL_SCL
Text GLabel 8350 4250 2    50   Input ~ 0
I2C_ISOL_SDA
$Comp
L spacemanic_power:GND_ISOL #PWR16
U 1 1 5E6435EE
P 8450 4450
F 0 "#PWR16" H 8625 4300 50  0001 C CNN
F 1 "GND_ISOL" V 8446 4322 50  0000 R CNN
F 2 "" H 8450 4450 50  0001 C CNN
F 3 "" H 8450 4450 50  0001 C CNN
	1    8450 4450
	0    -1   -1   0   
$EndComp
Text GLabel 8350 3050 2    50   Input ~ 0
GPIO0
Wire Wire Line
	8350 2950 8450 2950
Wire Wire Line
	8350 2650 8450 2650
Text GLabel 8350 2450 2    50   Input ~ 0
SPI_0_CLK
Text GLabel 8350 2550 2    50   Input ~ 0
SPI_0_MISO
Text GLabel 8350 3450 2    50   Input ~ 0
PWM1
Text GLabel 8350 3150 2    50   Input ~ 0
GPIO2
Text GLabel 8350 3350 2    50   Input ~ 0
GPIO6_ADC2
Text GLabel 8350 3250 2    50   Input ~ 0
GPIOO4_ADC0
$Comp
L power:+BATT #PWR0111
U 1 1 5E4E3F25
P 8450 1550
F 0 "#PWR0111" H 8450 1400 50  0001 C CNN
F 1 "+BATT" V 8465 1678 50  0000 L CNN
F 2 "" H 8450 1550 50  0001 C CNN
F 3 "" H 8450 1550 50  0001 C CNN
	1    8450 1550
	0    1    1    0   
$EndComp
$Comp
L power:+BATT #PWR0117
U 1 1 5E4DD784
P 7750 1550
F 0 "#PWR0117" H 7750 1400 50  0001 C CNN
F 1 "+BATT" V 7765 1678 50  0000 L CNN
F 2 "" H 7750 1550 50  0001 C CNN
F 3 "" H 7750 1550 50  0001 C CNN
	1    7750 1550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E5D3C2E
P 7750 2650
F 0 "#PWR0108" H 7750 2400 50  0001 C CNN
F 1 "GND" V 7755 2522 50  0000 R CNN
F 2 "" H 7750 2650 50  0001 C CNN
F 3 "" H 7750 2650 50  0001 C CNN
	1    7750 2650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5E5D6A55
P 8450 2950
F 0 "#PWR0112" H 8450 2700 50  0001 C CNN
F 1 "GND" V 8455 2822 50  0000 R CNN
F 2 "" H 8450 2950 50  0001 C CNN
F 3 "" H 8450 2950 50  0001 C CNN
	1    8450 2950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5E634BCA
P 8450 3850
F 0 "#PWR0120" H 8450 3600 50  0001 C CNN
F 1 "GND" V 8455 3722 50  0000 R CNN
F 2 "" H 8450 3850 50  0001 C CNN
F 3 "" H 8450 3850 50  0001 C CNN
	1    8450 3850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5E634BB3
P 7750 3850
F 0 "#PWR0119" H 7750 3600 50  0001 C CNN
F 1 "GND" V 7755 3722 50  0000 R CNN
F 2 "" H 7750 3850 50  0001 C CNN
F 3 "" H 7750 3850 50  0001 C CNN
	1    7750 3850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E5D6A3E
P 7750 2950
F 0 "#PWR0109" H 7750 2700 50  0001 C CNN
F 1 "GND" V 7755 2822 50  0000 R CNN
F 2 "" H 7750 2950 50  0001 C CNN
F 3 "" H 7750 2950 50  0001 C CNN
	1    7750 2950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5E585E13
P 7750 2050
F 0 "#PWR0118" H 7750 1800 50  0001 C CNN
F 1 "GND" V 7755 1922 50  0000 R CNN
F 2 "" H 7750 2050 50  0001 C CNN
F 3 "" H 7750 2050 50  0001 C CNN
	1    7750 2050
	0    1    1    0   
$EndComp
$Comp
L spacemanic_power:+3.3V_ISOL #PWR9
U 1 1 5E63DEF5
P 7725 4150
F 0 "#PWR9" H 7700 4075 50  0001 C CNN
F 1 "+3.3V_ISOL" V 7711 4278 50  0000 L CNN
F 2 "" H 7950 4525 50  0001 C CNN
F 3 "" H 7950 4525 50  0001 C CNN
	1    7725 4150
	0    -1   -1   0   
$EndComp
$Comp
L spacemanic_power:+3.3V_ISOL #PWR15
U 1 1 5E63E9D5
P 8450 4150
F 0 "#PWR15" H 8425 4075 50  0001 C CNN
F 1 "+3.3V_ISOL" V 8435 4278 50  0000 L CNN
F 2 "" H 8675 4525 50  0001 C CNN
F 3 "" H 8675 4525 50  0001 C CNN
	1    8450 4150
	0    1    1    0   
$EndComp
Text GLabel 8350 2750 2    50   Input ~ 0
CAN_H
Text GLabel 8350 2850 2    50   Input ~ 0
CAN_L
Wire Wire Line
	4025 4900 4025 4975
Wire Wire Line
	4025 4975 4075 4975
Wire Wire Line
	4575 4975 4625 4975
Wire Wire Line
	4625 4975 4625 4900
$Comp
L power:GND #PWR0126
U 1 1 619F9BAB
P 4575 5075
F 0 "#PWR0126" H 4575 4825 50  0001 C CNN
F 1 "GND" V 4580 4947 50  0000 R CNN
F 2 "" H 4575 5075 50  0001 C CNN
F 3 "" H 4575 5075 50  0001 C CNN
	1    4575 5075
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 619FC66C
P 4075 5075
F 0 "#PWR0127" H 4075 4825 50  0001 C CNN
F 1 "GND" V 4080 4947 50  0000 R CNN
F 2 "" H 4075 5075 50  0001 C CNN
F 3 "" H 4075 5075 50  0001 C CNN
	1    4075 5075
	0    1    1    0   
$EndComp
NoConn ~ 9825 1525
NoConn ~ 10325 1525
NoConn ~ 9825 1925
NoConn ~ 10325 1925
NoConn ~ 9825 1825
NoConn ~ 9825 1725
NoConn ~ 9825 1625
NoConn ~ 10325 1625
NoConn ~ 10325 1725
NoConn ~ 10325 1825
Wire Wire Line
	10325 2025 10425 2025
$Comp
L power:GND #PWR0128
U 1 1 61A1CAB6
P 10425 2025
F 0 "#PWR0128" H 10425 1775 50  0001 C CNN
F 1 "GND" V 10430 1897 50  0000 R CNN
F 2 "" H 10425 2025 50  0001 C CNN
F 3 "" H 10425 2025 50  0001 C CNN
	1    10425 2025
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9825 2025 9725 2025
$Comp
L power:GND #PWR0129
U 1 1 61A22064
P 9725 2025
F 0 "#PWR0129" H 9725 1775 50  0001 C CNN
F 1 "GND" V 9730 1897 50  0000 R CNN
F 2 "" H 9725 2025 50  0001 C CNN
F 3 "" H 9725 2025 50  0001 C CNN
	1    9725 2025
	0    1    1    0   
$EndComp
Wire Wire Line
	9825 2625 9725 2625
$Comp
L power:GND #PWR0130
U 1 1 61A24DFA
P 9725 2625
F 0 "#PWR0130" H 9725 2375 50  0001 C CNN
F 1 "GND" V 9730 2497 50  0000 R CNN
F 2 "" H 9725 2625 50  0001 C CNN
F 3 "" H 9725 2625 50  0001 C CNN
	1    9725 2625
	0    1    1    0   
$EndComp
Wire Wire Line
	10325 2625 10425 2625
$Comp
L power:GND #PWR0131
U 1 1 61A279F5
P 10425 2625
F 0 "#PWR0131" H 10425 2375 50  0001 C CNN
F 1 "GND" V 10430 2497 50  0000 R CNN
F 2 "" H 10425 2625 50  0001 C CNN
F 3 "" H 10425 2625 50  0001 C CNN
	1    10425 2625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10325 2925 10425 2925
$Comp
L power:GND #PWR0132
U 1 1 61A2A795
P 10425 2925
F 0 "#PWR0132" H 10425 2675 50  0001 C CNN
F 1 "GND" V 10430 2797 50  0000 R CNN
F 2 "" H 10425 2925 50  0001 C CNN
F 3 "" H 10425 2925 50  0001 C CNN
	1    10425 2925
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9825 2925 9725 2925
$Comp
L power:GND #PWR0133
U 1 1 61A2D79D
P 9725 2925
F 0 "#PWR0133" H 9725 2675 50  0001 C CNN
F 1 "GND" V 9730 2797 50  0000 R CNN
F 2 "" H 9725 2925 50  0001 C CNN
F 3 "" H 9725 2925 50  0001 C CNN
	1    9725 2925
	0    1    1    0   
$EndComp
Wire Wire Line
	9825 3825 9725 3825
$Comp
L power:GND #PWR0134
U 1 1 61A308CF
P 9725 3825
F 0 "#PWR0134" H 9725 3575 50  0001 C CNN
F 1 "GND" V 9730 3697 50  0000 R CNN
F 2 "" H 9725 3825 50  0001 C CNN
F 3 "" H 9725 3825 50  0001 C CNN
	1    9725 3825
	0    1    1    0   
$EndComp
Wire Wire Line
	10325 3825 10425 3825
$Comp
L power:GND #PWR0135
U 1 1 61A36617
P 10425 3825
F 0 "#PWR0135" H 10425 3575 50  0001 C CNN
F 1 "GND" V 10430 3697 50  0000 R CNN
F 2 "" H 10425 3825 50  0001 C CNN
F 3 "" H 10425 3825 50  0001 C CNN
	1    10425 3825
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9825 4425 9725 4425
$Comp
L power:GND #PWR0136
U 1 1 61A396D9
P 9725 4425
F 0 "#PWR0136" H 9725 4175 50  0001 C CNN
F 1 "GND" V 9730 4297 50  0000 R CNN
F 2 "" H 9725 4425 50  0001 C CNN
F 3 "" H 9725 4425 50  0001 C CNN
	1    9725 4425
	0    1    1    0   
$EndComp
Wire Wire Line
	10325 4425 10425 4425
$Comp
L power:GND #PWR0137
U 1 1 61A3CB81
P 10425 4425
F 0 "#PWR0137" H 10425 4175 50  0001 C CNN
F 1 "GND" V 10430 4297 50  0000 R CNN
F 2 "" H 10425 4425 50  0001 C CNN
F 3 "" H 10425 4425 50  0001 C CNN
	1    10425 4425
	0    -1   -1   0   
$EndComp
NoConn ~ 9825 2725
NoConn ~ 9825 2825
NoConn ~ 10325 3625
NoConn ~ 10325 3725
NoConn ~ 9825 3525
NoConn ~ 9825 3625
NoConn ~ 9825 3725
NoConn ~ 9825 3925
NoConn ~ 9825 4225
NoConn ~ 9825 4325
Text GLabel 9750 2125 0    50   Input ~ 0
TXEN
Wire Wire Line
	9750 2125 9825 2125
Text GLabel 9725 2225 0    50   Output ~ 0
RXD1
Wire Wire Line
	9725 2225 9825 2225
Text GLabel 9750 2325 0    50   Input ~ 0
TXD1
Wire Wire Line
	9750 2325 9825 2325
Text GLabel 9725 2425 0    50   Output ~ 0
RXER
Wire Wire Line
	9725 2425 9825 2425
Text GLabel 9750 2525 0    50   BiDi ~ 0
MDIO
Wire Wire Line
	9750 2525 9825 2525
Text GLabel 10400 2125 2    50   Output ~ 0
REFCLK
Wire Wire Line
	10400 2125 10325 2125
Text GLabel 10400 2225 2    50   Output ~ 0
RXD0
Wire Wire Line
	10400 2225 10325 2225
Text GLabel 10375 2325 2    50   Input ~ 0
TXD0
Wire Wire Line
	10375 2325 10325 2325
Text GLabel 10400 2425 2    50   Output ~ 0
CRSDV
Wire Wire Line
	10400 2425 10325 2425
Text GLabel 10375 2525 2    50   Input ~ 0
MDC
Wire Wire Line
	10375 2525 10325 2525
Text GLabel 4025 5175 0    50   Output ~ 0
MDC
Wire Wire Line
	4025 5175 4075 5175
Text GLabel 4600 5175 2    50   BiDi ~ 0
MDIO
Wire Wire Line
	4600 5175 4575 5175
Text GLabel 4050 5275 0    50   Input ~ 0
CRSDV
Wire Wire Line
	4050 5275 4075 5275
Text GLabel 4600 5475 2    50   Input ~ 0
TXEN
Wire Wire Line
	4600 5475 4575 5475
Text GLabel 4050 5375 0    50   Input ~ 0
RXD1
Wire Wire Line
	4050 5375 4075 5375
Text GLabel 4025 5475 0    50   Output ~ 0
TXD0
Wire Wire Line
	4025 5475 4075 5475
Text GLabel 4600 5375 2    50   Input ~ 0
RXD0
Wire Wire Line
	4600 5375 4575 5375
Text GLabel 4625 5575 2    50   Output ~ 0
TXD1
Wire Wire Line
	4625 5575 4575 5575
Text GLabel 4600 5275 2    50   Input ~ 0
REFCLK
Wire Wire Line
	4600 5275 4575 5275
Text GLabel 10375 2725 2    50   BiDi ~ 0
USB_HSD_N
Wire Wire Line
	10375 2725 10325 2725
Text GLabel 10375 2825 2    50   BiDi ~ 0
USB_HSD_P
Wire Wire Line
	10375 2825 10325 2825
Text GLabel 10375 3025 2    50   BiDi ~ 0
GPIO8
Text GLabel 10375 3125 2    50   BiDi ~ 0
GPIO10
Wire Wire Line
	10375 3025 10325 3025
Wire Wire Line
	10375 3125 10325 3125
Text GLabel 9775 3325 0    50   BiDi ~ 0
GPIO15
Text GLabel 9775 3225 0    50   BiDi ~ 0
GPIO13
Wire Wire Line
	9775 3325 9825 3325
Wire Wire Line
	9775 3225 9825 3225
Text GLabel 9775 3125 0    50   BiDi ~ 0
GPIO11
Text GLabel 9775 3025 0    50   BiDi ~ 0
GPIO9
Wire Wire Line
	9775 3125 9825 3125
Wire Wire Line
	9775 3025 9825 3025
Wire Wire Line
	10375 3325 10325 3325
Wire Wire Line
	10375 3225 10325 3225
Text GLabel 10375 3325 2    50   BiDi ~ 0
GPIO14
Text GLabel 10375 3225 2    50   BiDi ~ 0
GPIO12
Wire Wire Line
	10400 3525 10325 3525
Wire Wire Line
	10400 3425 10325 3425
Text GLabel 10400 3525 2    50   Output ~ 0
PWM_L2
Text GLabel 10400 3425 2    50   Output ~ 0
PWM_L0
Wire Wire Line
	9750 3425 9825 3425
Text GLabel 9750 3425 0    50   Output ~ 0
PWM_L1
Text GLabel 9775 4125 0    50   BiDi ~ 0
FLASH_RST#_IO3
Text GLabel 9775 4025 0    50   BiDi ~ 0
FLASH_MISO_IO1
Wire Wire Line
	9775 4125 9825 4125
Wire Wire Line
	9775 4025 9825 4025
Text GLabel 10375 3925 2    50   Input ~ 0
FLASH_CLK
Text GLabel 10375 4025 2    50   BiDi ~ 0
FLASH_MOSI_IO0
Wire Wire Line
	10375 3925 10325 3925
Wire Wire Line
	10375 4025 10325 4025
Text GLabel 10375 4125 2    50   BiDi ~ 0
FLASH_WP#_IO2
Text GLabel 10375 4225 2    50   BiDi ~ 0
I2C_SENS_SDA
Wire Wire Line
	10375 4125 10325 4125
Wire Wire Line
	10375 4225 10325 4225
Text GLabel 10400 4325 2    50   Output ~ 0
I2C_SENS_SCL
Wire Wire Line
	10400 4325 10325 4325
$Comp
L power:GND #PWR0138
U 1 1 61A73925
P 6200 5700
F 0 "#PWR0138" H 6200 5450 50  0001 C CNN
F 1 "GND" H 6275 5525 50  0000 R CNN
F 2 "" H 6200 5700 50  0001 C CNN
F 3 "" H 6200 5700 50  0001 C CNN
	1    6200 5700
	1    0    0    -1  
$EndComp
Text GLabel 7900 5300 2    50   BiDi ~ 0
USB_HSD_N
Text GLabel 7900 5200 2    50   BiDi ~ 0
USB_HSD_P
Wire Wire Line
	7900 5200 6800 5200
Wire Wire Line
	7900 5300 7800 5300
Wire Wire Line
	4550 2500 4650 2500
Wire Wire Line
	4050 2500 3950 2500
Wire Wire Line
	4050 3400 3950 3400
$Comp
L power:GND #PWR0139
U 1 1 61B08166
P 3950 3400
F 0 "#PWR0139" H 3950 3150 50  0001 C CNN
F 1 "GND" V 3955 3272 50  0000 R CNN
F 2 "" H 3950 3400 50  0001 C CNN
F 3 "" H 3950 3400 50  0001 C CNN
	1    3950 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 3400 4650 3400
$Comp
L power:GND #PWR0140
U 1 1 61B0816D
P 4650 3400
F 0 "#PWR0140" H 4650 3150 50  0001 C CNN
F 1 "GND" V 4655 3272 50  0000 R CNN
F 2 "" H 4650 3400 50  0001 C CNN
F 3 "" H 4650 3400 50  0001 C CNN
	1    4650 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 4000 3950 4000
$Comp
L power:GND #PWR0141
U 1 1 61B08174
P 3950 4000
F 0 "#PWR0141" H 3950 3750 50  0001 C CNN
F 1 "GND" V 3955 3872 50  0000 R CNN
F 2 "" H 3950 4000 50  0001 C CNN
F 3 "" H 3950 4000 50  0001 C CNN
	1    3950 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 4000 4650 4000
$Comp
L power:GND #PWR0142
U 1 1 61B0817B
P 4650 4000
F 0 "#PWR0142" H 4650 3750 50  0001 C CNN
F 1 "GND" V 4655 3872 50  0000 R CNN
F 2 "" H 4650 4000 50  0001 C CNN
F 3 "" H 4650 4000 50  0001 C CNN
	1    4650 4000
	0    -1   -1   0   
$EndComp
NoConn ~ 4550 3200
NoConn ~ 4550 3300
NoConn ~ 4050 3100
NoConn ~ 4050 3200
NoConn ~ 4050 3300
NoConn ~ 4050 3500
NoConn ~ 4050 3800
NoConn ~ 4050 3900
Text GLabel 4600 2600 2    50   BiDi ~ 0
GPIO8
Text GLabel 4600 2700 2    50   BiDi ~ 0
GPIO10
Wire Wire Line
	4600 2600 4550 2600
Wire Wire Line
	4600 2700 4550 2700
Text GLabel 4000 2900 0    50   BiDi ~ 0
GPIO15
Text GLabel 4000 2800 0    50   BiDi ~ 0
GPIO13
Wire Wire Line
	4000 2900 4050 2900
Wire Wire Line
	4000 2800 4050 2800
Text GLabel 4000 2700 0    50   BiDi ~ 0
GPIO11
Text GLabel 4000 2600 0    50   BiDi ~ 0
GPIO9
Wire Wire Line
	4000 2700 4050 2700
Wire Wire Line
	4000 2600 4050 2600
Wire Wire Line
	4600 2900 4550 2900
Wire Wire Line
	4600 2800 4550 2800
Text GLabel 4600 2900 2    50   BiDi ~ 0
GPIO14
Text GLabel 4600 2800 2    50   BiDi ~ 0
GPIO12
Wire Wire Line
	4625 3100 4550 3100
Wire Wire Line
	4625 3000 4550 3000
Text GLabel 4625 3100 2    50   Output ~ 0
PWM_L2
Text GLabel 4625 3000 2    50   Output ~ 0
PWM_L0
Wire Wire Line
	3975 3000 4050 3000
Text GLabel 3975 3000 0    50   Output ~ 0
PWM_L1
Text GLabel 4000 3700 0    50   BiDi ~ 0
FLASH_RST#_IO3
Text GLabel 4000 3600 0    50   BiDi ~ 0
FLASH_MISO_IO1
Wire Wire Line
	4000 3700 4050 3700
Wire Wire Line
	4000 3600 4050 3600
Text GLabel 4600 3500 2    50   Input ~ 0
FLASH_CLK
Text GLabel 4600 3600 2    50   BiDi ~ 0
FLASH_MOSI_IO0
Wire Wire Line
	4600 3500 4550 3500
Wire Wire Line
	4600 3600 4550 3600
Text GLabel 4600 3700 2    50   BiDi ~ 0
FLASH_WP#_IO2
Text GLabel 4600 3800 2    50   BiDi ~ 0
I2C_SENS_SDA
Wire Wire Line
	4600 3700 4550 3700
Wire Wire Line
	4600 3800 4550 3800
Text GLabel 4625 3900 2    50   Output ~ 0
I2C_SENS_SCL
Wire Wire Line
	4625 3900 4550 3900
$Comp
L power:GND #PWR0143
U 1 1 61B0815F
P 3950 2500
F 0 "#PWR0143" H 3950 2250 50  0001 C CNN
F 1 "GND" V 3955 2372 50  0000 R CNN
F 2 "" H 3950 2500 50  0001 C CNN
F 3 "" H 3950 2500 50  0001 C CNN
	1    3950 2500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0144
U 1 1 61B08158
P 4650 2500
F 0 "#PWR0144" H 4650 2250 50  0001 C CNN
F 1 "GND" V 4655 2372 50  0000 R CNN
F 2 "" H 4650 2500 50  0001 C CNN
F 3 "" H 4650 2500 50  0001 C CNN
	1    4650 2500
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_02x16_Odd_Even J6
U 1 1 61B0812B
P 4350 3200
F 0 "J6" H 4400 4050 60  0000 C CNN
F 1 "HEADER_2x30" H 4350 4897 60  0001 C CNN
F 2 "Spacemanic_header:Header_2x16_2.54mm" H 4350 4790 60  0001 C CNN
F 3 "" H 4350 4650 60  0000 C CNN
F 4 "TSW-116-07-x-D" H 4350 3200 50  0001 C CNN "MFPN"
	1    4350 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6500 5000 7300 5000
NoConn ~ 4075 5575
Text GLabel 3825 1950 0    50   Input ~ 0
2.32
Text GLabel 3825 1750 0    50   Input ~ 0
2.29
Text GLabel 3825 1850 0    50   Input ~ 0
2.30
Text GLabel 2925 2450 2    50   Input ~ 0
1.26
Text GLabel 2925 2550 2    50   Input ~ 0
1.28
Text GLabel 2925 2350 2    50   Input ~ 0
1.24
Text GLabel 2925 2250 2    50   Input ~ 0
1.22
Text GLabel 2925 1450 2    50   Input ~ 0
1.6
Text GLabel 2925 1350 2    50   Input ~ 0
1.4
Text GLabel 2925 1250 2    50   Input ~ 0
1.2
Text GLabel 2925 1550 2    50   Input ~ 0
1.8
Text GLabel 2925 1650 2    50   Input ~ 0
1.10
Text GLabel 3825 1525 0    50   Input ~ 0
2.46
Text GLabel 3825 1425 0    50   Input ~ 0
2.45
Text GLabel 2925 1750 2    50   Input ~ 0
1.12
Text GLabel 2925 1850 2    50   Input ~ 0
1.14
Text GLabel 2925 2050 2    50   Input ~ 0
1.18
Text GLabel 2925 1950 2    50   Input ~ 0
1.16
Text GLabel 2925 2150 2    50   Input ~ 0
1.20
Text GLabel 2925 2650 2    50   Input ~ 0
1.30
Text GLabel 2925 2750 2    50   Input ~ 0
1.32
Text GLabel 2925 2950 2    50   Input ~ 0
1.36
Text GLabel 2925 2850 2    50   Input ~ 0
1.34
Text GLabel 2425 2650 0    50   Input ~ 0
1.29
Text GLabel 2425 2750 0    50   Input ~ 0
1.31
Text GLabel 2425 2850 0    50   Input ~ 0
1.33
Text GLabel 2425 3050 0    50   Input ~ 0
1.37
Text GLabel 2425 2950 0    50   Input ~ 0
1.35
Text GLabel 2425 1250 0    50   Input ~ 0
1.1
Text GLabel 2425 1650 0    50   Input ~ 0
1.9
Text GLabel 2425 1550 0    50   Input ~ 0
1.7
Text GLabel 2425 1450 0    50   Input ~ 0
1.5
Text GLabel 2425 1350 0    50   Input ~ 0
1.3
Text GLabel 2425 1950 0    50   Input ~ 0
1.15
Text GLabel 2425 2050 0    50   Input ~ 0
1.17
Text GLabel 2425 2150 0    50   Input ~ 0
1.19
Text GLabel 2425 2250 0    50   Input ~ 0
1.21
Text GLabel 2425 2350 0    50   Input ~ 0
1.23
Text GLabel 2425 2450 0    50   Input ~ 0
1.25
Text GLabel 2425 2550 0    50   Input ~ 0
1.27
Text GLabel 2425 1850 0    50   Input ~ 0
1.13
Text GLabel 2425 1750 0    50   Input ~ 0
1.11
Text GLabel 1525 1650 2    50   Input ~ 0
1.10
Text GLabel 1525 1550 2    50   Input ~ 0
1.8
Text GLabel 1525 1450 2    50   Input ~ 0
1.6
Text GLabel 1025 1550 0    50   Input ~ 0
1.7
Text GLabel 1025 1350 0    50   Input ~ 0
1.3
Text GLabel 1025 1650 0    50   Input ~ 0
1.9
Text GLabel 1025 1250 0    50   Input ~ 0
1.1
Text GLabel 1525 1250 2    50   Input ~ 0
1.2
Text GLabel 1525 1350 2    50   Input ~ 0
1.4
Text GLabel 1025 1450 0    50   Input ~ 0
1.5
Text GLabel 1525 1950 2    50   Input ~ 0
1.16
Text GLabel 1525 1850 2    50   Input ~ 0
1.14
Text GLabel 1525 1750 2    50   Input ~ 0
1.12
Text GLabel 1525 2050 2    50   Input ~ 0
1.18
Text GLabel 1525 2150 2    50   Input ~ 0
1.20
Text GLabel 1025 1750 0    50   Input ~ 0
1.11
Text GLabel 1025 2050 0    50   Input ~ 0
1.17
Text GLabel 1025 1950 0    50   Input ~ 0
1.15
Text GLabel 1025 1850 0    50   Input ~ 0
1.13
Text GLabel 1025 2150 0    50   Input ~ 0
1.19
Text GLabel 1025 2250 0    50   Input ~ 0
1.21
Text GLabel 1025 2350 0    50   Input ~ 0
1.23
Text GLabel 1025 2450 0    50   Input ~ 0
1.25
Text GLabel 1025 2550 0    50   Input ~ 0
1.27
Text GLabel 1025 2650 0    50   Input ~ 0
1.29
Text GLabel 1525 2250 2    50   Input ~ 0
1.22
Text GLabel 1525 2450 2    50   Input ~ 0
1.26
Text GLabel 1525 2350 2    50   Input ~ 0
1.24
Text GLabel 1525 2550 2    50   Input ~ 0
1.28
Text GLabel 1525 2650 2    50   Input ~ 0
1.30
Text GLabel 1525 2750 2    50   Input ~ 0
1.32
Text GLabel 1025 2750 0    50   Input ~ 0
1.31
Text GLabel 1525 2850 2    50   Input ~ 0
1.34
Text GLabel 1525 2950 2    50   Input ~ 0
1.36
Text GLabel 1025 2950 0    50   Input ~ 0
1.35
Text GLabel 1025 2850 0    50   Input ~ 0
1.33
Text GLabel 1025 3150 0    50   Input ~ 0
1.39
Text GLabel 1025 3050 0    50   Input ~ 0
1.37
Text GLabel 1025 3250 0    50   Input ~ 0
1.41
Text GLabel 1025 3350 0    50   Input ~ 0
1.43
Text GLabel 1025 3450 0    50   Input ~ 0
1.45
Text GLabel 2925 4800 2    50   Input ~ 0
2.12
Text GLabel 2925 4700 2    50   Input ~ 0
2.10
Text GLabel 2925 4600 2    50   Input ~ 0
2.8
Text GLabel 2925 4500 2    50   Input ~ 0
2.6
Text GLabel 2925 4300 2    50   Input ~ 0
2.2
Text GLabel 2925 5000 2    50   Input ~ 0
2.16
Text GLabel 2925 4900 2    50   Input ~ 0
2.14
Text GLabel 2925 5600 2    50   Input ~ 0
2.28
Text GLabel 2925 5100 2    50   Input ~ 0
2.18
Text GLabel 2925 5500 2    50   Input ~ 0
2.26
Text GLabel 2925 5200 2    50   Input ~ 0
2.20
Text GLabel 2925 5400 2    50   Input ~ 0
2.24
Text GLabel 2925 5300 2    50   Input ~ 0
2.22
Text GLabel 2425 4600 0    50   Input ~ 0
2.7
Text GLabel 2425 4800 0    50   Input ~ 0
2.11
Text GLabel 2425 4700 0    50   Input ~ 0
2.9
Text GLabel 2425 4900 0    50   Input ~ 0
2.13
Text GLabel 2425 5000 0    50   Input ~ 0
2.15
Text GLabel 2425 5100 0    50   Input ~ 0
2.17
Text GLabel 2425 5300 0    50   Input ~ 0
2.21
Text GLabel 2425 5200 0    50   Input ~ 0
2.19
Text GLabel 2925 5700 2    50   Input ~ 0
2.30
Text GLabel 2425 5500 0    50   Input ~ 0
2.25
Text GLabel 2425 5400 0    50   Input ~ 0
2.23
Text GLabel 2425 5600 0    50   Input ~ 0
2.27
Text GLabel 2425 5700 0    50   Input ~ 0
2.29
Text GLabel 2925 5800 2    50   Input ~ 0
2.32
Text GLabel 2925 5900 2    50   Input ~ 0
2.34
Text GLabel 2925 6000 2    50   Input ~ 0
2.36
Text GLabel 2925 6100 2    50   Input ~ 0
2.38
Text GLabel 2925 6200 2    50   Input ~ 0
2.40
Text GLabel 1525 6800 2    50   Input ~ 0
2.52
Text GLabel 1525 6700 2    50   Input ~ 0
2.50
Text GLabel 1525 6600 2    50   Input ~ 0
2.48
Text GLabel 1525 6500 2    50   Input ~ 0
2.46
Text GLabel 1525 6400 2    50   Input ~ 0
2.44
Text GLabel 1525 6000 2    50   Input ~ 0
2.36
Text GLabel 1525 5900 2    50   Input ~ 0
2.34
Text GLabel 1525 6300 2    50   Input ~ 0
2.42
Text GLabel 2425 6000 0    50   Input ~ 0
2.35
Text GLabel 2425 5900 0    50   Input ~ 0
2.33
Text GLabel 2925 6300 2    50   Input ~ 0
2.42
Text GLabel 2925 6400 2    50   Input ~ 0
2.44
Text GLabel 2925 6500 2    50   Input ~ 0
2.46
Text GLabel 2925 6600 2    50   Input ~ 0
2.48
Text GLabel 2925 6700 2    50   Input ~ 0
2.50
Text GLabel 2925 6800 2    50   Input ~ 0
2.52
Text GLabel 2425 6800 0    50   Input ~ 0
2.51
Text GLabel 2425 6700 0    50   Input ~ 0
2.49
Text GLabel 2425 6600 0    50   Input ~ 0
2.47
Text GLabel 2425 5800 0    50   Input ~ 0
2.31
Text GLabel 2425 6500 0    50   Input ~ 0
2.45
Text GLabel 2425 6400 0    50   Input ~ 0
2.43
Text GLabel 2425 6300 0    50   Input ~ 0
2.41
Text GLabel 2425 6200 0    50   Input ~ 0
2.39
Text GLabel 2425 6100 0    50   Input ~ 0
2.37
Text GLabel 1525 5600 2    50   Input ~ 0
2.28
Text GLabel 1525 5500 2    50   Input ~ 0
2.26
Text GLabel 1525 5400 2    50   Input ~ 0
2.24
Text GLabel 1525 4900 2    50   Input ~ 0
2.14
Text GLabel 1525 5000 2    50   Input ~ 0
2.16
Text GLabel 1525 5100 2    50   Input ~ 0
2.18
Text GLabel 1525 5300 2    50   Input ~ 0
2.22
Text GLabel 1525 5200 2    50   Input ~ 0
2.20
Text GLabel 1525 5700 2    50   Input ~ 0
2.30
Text GLabel 1525 5800 2    50   Input ~ 0
2.32
Text GLabel 1525 6200 2    50   Input ~ 0
2.40
Text GLabel 1525 6100 2    50   Input ~ 0
2.38
Text GLabel 1025 6000 0    50   Input ~ 0
2.35
Text GLabel 1025 5900 0    50   Input ~ 0
2.33
Text GLabel 1025 5700 0    50   Input ~ 0
2.29
Text GLabel 1025 5800 0    50   Input ~ 0
2.31
Text GLabel 1025 5500 0    50   Input ~ 0
2.25
Text GLabel 1025 5400 0    50   Input ~ 0
2.23
Text GLabel 1025 5600 0    50   Input ~ 0
2.27
Text GLabel 1025 6100 0    50   Input ~ 0
2.37
Text GLabel 1025 6200 0    50   Input ~ 0
2.39
Text GLabel 1025 6300 0    50   Input ~ 0
2.41
Text GLabel 1025 6400 0    50   Input ~ 0
2.43
Text GLabel 1025 6500 0    50   Input ~ 0
2.45
Text GLabel 1025 6600 0    50   Input ~ 0
2.47
Text GLabel 1025 6700 0    50   Input ~ 0
2.49
Text GLabel 1025 6800 0    50   Input ~ 0
2.51
Text GLabel 1025 4900 0    50   Input ~ 0
2.13
Text GLabel 1025 5000 0    50   Input ~ 0
2.15
Text GLabel 1025 5100 0    50   Input ~ 0
2.17
Text GLabel 1025 5200 0    50   Input ~ 0
2.19
Text GLabel 1025 5300 0    50   Input ~ 0
2.21
Text GLabel 2925 3350 2    50   Input ~ 0
1.44
Text GLabel 2925 3550 2    50   Input ~ 0
1.48
Text GLabel 2925 3650 2    50   Input ~ 0
1.50
Text GLabel 2925 3750 2    50   Input ~ 0
1.52
Text GLabel 2925 3450 2    50   Input ~ 0
1.46
Text GLabel 2925 3050 2    50   Input ~ 0
1.38
Text GLabel 2425 3150 0    50   Input ~ 0
1.39
Text GLabel 2925 3150 2    50   Input ~ 0
1.40
Text GLabel 2925 3250 2    50   Input ~ 0
1.42
Text GLabel 2425 3250 0    50   Input ~ 0
1.41
Text GLabel 2425 3350 0    50   Input ~ 0
1.43
Text GLabel 2425 3550 0    50   Input ~ 0
1.47
Text GLabel 2425 3650 0    50   Input ~ 0
1.49
Text GLabel 2425 3450 0    50   Input ~ 0
1.45
Text GLabel 2925 4400 2    50   Input ~ 0
2.4
Text GLabel 2425 4500 0    50   Input ~ 0
2.5
Text GLabel 2425 4400 0    50   Input ~ 0
2.3
Text GLabel 2425 4300 0    50   Input ~ 0
2.1
Text GLabel 2425 3750 0    50   Input ~ 0
1.51
Text GLabel 1525 3450 2    50   Input ~ 0
1.46
Text GLabel 1525 3150 2    50   Input ~ 0
1.40
Text GLabel 1525 3050 2    50   Input ~ 0
1.38
Text GLabel 1525 3250 2    50   Input ~ 0
1.42
Text GLabel 1525 3350 2    50   Input ~ 0
1.44
Text GLabel 1025 3650 0    50   Input ~ 0
1.49
Text GLabel 1025 3550 0    50   Input ~ 0
1.47
Text GLabel 1025 3750 0    50   Input ~ 0
1.51
Text GLabel 1025 4300 0    50   Input ~ 0
2.1
Text GLabel 1525 3650 2    50   Input ~ 0
1.50
Text GLabel 1525 3550 2    50   Input ~ 0
1.48
Text GLabel 1525 3750 2    50   Input ~ 0
1.52
Text GLabel 1525 4400 2    50   Input ~ 0
2.4
Text GLabel 1025 4800 0    50   Input ~ 0
2.11
Text GLabel 1025 4600 0    50   Input ~ 0
2.7
Text GLabel 1025 4500 0    50   Input ~ 0
2.5
Text GLabel 1025 4400 0    50   Input ~ 0
2.3
Text GLabel 1025 4700 0    50   Input ~ 0
2.9
Text GLabel 1525 4600 2    50   Input ~ 0
2.8
Text GLabel 1525 4800 2    50   Input ~ 0
2.12
Text GLabel 1525 4700 2    50   Input ~ 0
2.10
Text GLabel 1525 4500 2    50   Input ~ 0
2.6
Text GLabel 1525 4300 2    50   Input ~ 0
2.2
Text Notes 3975 5800 0    59   ~ 12
ETHERNET RMII
$Comp
L Power_Protection:PRTR5V0U2X D1
U 1 1 61AEE2FE
P 7300 5825
F 0 "D1" H 7025 6175 50  0000 L CNN
F 1 "PRTR5V0U2X" H 7375 5475 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-143" H 7360 5825 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/PRTR5V0U2X.pdf" H 7360 5825 50  0001 C CNN
F 4 "PRTR5V0U2X" H 7300 5825 50  0001 C CNN "MFPN"
	1    7300 5825
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5600 6200 5650
Wire Wire Line
	6800 5825 6800 5200
Connection ~ 6800 5200
Wire Wire Line
	6800 5200 6500 5200
Wire Wire Line
	7800 5825 7800 5300
Connection ~ 7800 5300
Wire Wire Line
	7800 5300 6500 5300
$Comp
L power:GND #PWR0150
U 1 1 61BA3AFE
P 7300 6400
F 0 "#PWR0150" H 7300 6150 50  0001 C CNN
F 1 "GND" H 7375 6225 50  0000 R CNN
F 2 "" H 7300 6400 50  0001 C CNN
F 3 "" H 7300 6400 50  0001 C CNN
	1    7300 6400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0151
U 1 1 61C2E7B2
P 4200 1125
F 0 "#PWR0151" H 4200 975 50  0001 C CNN
F 1 "+5V" H 4215 1298 50  0000 C CNN
F 2 "" H 4200 1125 50  0001 C CNN
F 3 "" H 4200 1125 50  0001 C CNN
	1    4200 1125
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0152
U 1 1 61C48F82
P 5850 1750
F 0 "#PWR0152" H 5850 1600 50  0001 C CNN
F 1 "+5V" V 5850 1950 50  0000 C CNN
F 2 "" H 5850 1750 50  0001 C CNN
F 3 "" H 5850 1750 50  0001 C CNN
	1    5850 1750
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0153
U 1 1 61C5639C
P 6550 1750
F 0 "#PWR0153" H 6550 1600 50  0001 C CNN
F 1 "+5V" V 6550 1950 50  0000 C CNN
F 2 "" H 6550 1750 50  0001 C CNN
F 3 "" H 6550 1750 50  0001 C CNN
	1    6550 1750
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0154
U 1 1 61C63924
P 7750 1750
F 0 "#PWR0154" H 7750 1600 50  0001 C CNN
F 1 "+5V" V 7750 1950 50  0000 C CNN
F 2 "" H 7750 1750 50  0001 C CNN
F 3 "" H 7750 1750 50  0001 C CNN
	1    7750 1750
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0155
U 1 1 61C70D31
P 8450 1750
F 0 "#PWR0155" H 8450 1600 50  0001 C CNN
F 1 "+5V" V 8450 1950 50  0000 C CNN
F 2 "" H 8450 1750 50  0001 C CNN
F 3 "" H 8450 1750 50  0001 C CNN
	1    8450 1750
	0    1    1    0   
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP4
U 1 1 61C7E4E6
P 6650 5975
F 0 "JP4" V 6650 6043 50  0000 L CNN
F 1 "SolderJumper_2_Open" H 6650 6089 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6650 5975 50  0001 C CNN
F 3 "~" H 6650 5975 50  0001 C CNN
	1    6650 5975
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 6325 7300 6375
Wire Wire Line
	6650 6125 6650 6375
Wire Wire Line
	6650 6375 7300 6375
Connection ~ 7300 6375
Wire Wire Line
	7300 6375 7300 6400
$Comp
L power:+3.3V #PWR0156
U 1 1 61BF9394
P 10375 4975
F 0 "#PWR0156" H 10375 4825 50  0001 C CNN
F 1 "+3.3V" H 10390 5148 50  0000 C CNN
F 2 "" H 10375 4975 50  0001 C CNN
F 3 "" H 10375 4975 50  0001 C CNN
	1    10375 4975
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0157
U 1 1 61BF762A
P 8700 4975
F 0 "#PWR0157" H 8700 4825 50  0001 C CNN
F 1 "+5V" H 8715 5148 50  0000 C CNN
F 2 "" H 8700 4975 50  0001 C CNN
F 3 "" H 8700 4975 50  0001 C CNN
	1    8700 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	9525 5425 9525 5875
Wire Wire Line
	10375 5425 10375 4975
Connection ~ 10375 5425
Wire Wire Line
	9525 5425 10375 5425
Wire Wire Line
	9425 5800 9950 5800
Wire Wire Line
	10375 5450 10375 5425
Connection ~ 10375 5800
Wire Wire Line
	10375 5750 10375 5800
Wire Wire Line
	8700 4975 8700 5450
Wire Wire Line
	8700 5750 8700 5800
$Comp
L Device:D_Schottky D3
U 1 1 61DCEDED
P 10375 5600
F 0 "D3" H 10375 5725 50  0000 C CNN
F 1 "D_Small_ALT" H 10375 5726 50  0001 C CNN
F 2 "Spacemanic_SMD:D_SOD323_HS" V 10375 5600 50  0001 C CNN
F 3 "~" V 10375 5600 50  0001 C CNN
F 4 "BAT60AE6327HTSA1" H 10375 5600 50  0001 C CNN "MFPN"
	1    10375 5600
	0    1    1    0   
$EndComp
$Comp
L Device:D_Schottky D2
U 1 1 61DCE406
P 8700 5600
F 0 "D2" V 8700 5680 50  0000 L CNN
F 1 "D_Small_ALT" V 8745 5680 50  0001 L CNN
F 2 "Spacemanic_SMD:D_SOD323_HS" V 8700 5600 50  0001 C CNN
F 3 "~" V 8700 5600 50  0001 C CNN
F 4 "BAT60AE6327HTSA1" H 8700 5600 50  0001 C CNN "MFPN"
	1    8700 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	10375 6225 10375 6400
$Comp
L power:GND #PWR0149
U 1 1 61D583D8
P 10375 6400
F 0 "#PWR0149" H 10375 6150 50  0001 C CNN
F 1 "GND" H 10450 6250 50  0000 R CNN
F 2 "" H 10375 6400 50  0001 C CNN
F 3 "" H 10375 6400 50  0001 C CNN
	1    10375 6400
	1    0    0    -1  
$EndComp
Connection ~ 9950 5800
Wire Wire Line
	10375 5800 9950 5800
Wire Wire Line
	10375 6025 10375 5800
Wire Wire Line
	8700 6400 8700 6225
$Comp
L power:GND #PWR0148
U 1 1 61D2DBED
P 8700 6400
F 0 "#PWR0148" H 8700 6150 50  0001 C CNN
F 1 "GND" H 8775 6250 50  0000 R CNN
F 2 "" H 8700 6400 50  0001 C CNN
F 3 "" H 8700 6400 50  0001 C CNN
	1    8700 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 5800 8825 5800
Connection ~ 8700 5800
Wire Wire Line
	8700 6025 8700 5800
$Comp
L Device:CP1_Small C1
U 1 1 61D2043A
P 8700 6125
F 0 "C1" H 8500 6125 50  0000 L CNN
F 1 "10uF" H 8450 6050 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B_Pad1.50x2.35mm_HandSolder" H 8700 6125 50  0001 C CNN
F 3 "~" H 8700 6125 50  0001 C CNN
F 4 "" H 8700 6125 50  0001 C CNN "MFPN"
	1    8700 6125
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C4
U 1 1 61D1FB0D
P 10375 6125
F 0 "C4" H 10466 6171 50  0000 L CNN
F 1 "10uF" H 10466 6080 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B_Pad1.50x2.35mm_HandSolder" H 10375 6125 50  0001 C CNN
F 3 "~" H 10375 6125 50  0001 C CNN
F 4 "" H 10375 6125 50  0001 C CNN "MFPN"
	1    10375 6125
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 6225 9950 6400
$Comp
L power:GND #PWR0147
U 1 1 61CEC1AD
P 9950 6400
F 0 "#PWR0147" H 9950 6150 50  0001 C CNN
F 1 "GND" H 10025 6250 50  0000 R CNN
F 2 "" H 9950 6400 50  0001 C CNN
F 3 "" H 9950 6400 50  0001 C CNN
	1    9950 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 6025 9950 5800
$Comp
L Device:C_Small C3
U 1 1 61CD686C
P 9950 6125
F 0 "C3" H 9850 6075 50  0000 R CNN
F 1 "100nF" H 9850 6175 50  0000 R CNN
F 2 "Spacemanic_SMD:C_0603_HS" H 9950 6125 50  0001 C CNN
F 3 "~" H 9950 6125 50  0001 C CNN
	1    9950 6125
	-1   0    0    1   
$EndComp
Connection ~ 9125 6125
Wire Wire Line
	9125 6200 9125 6125
$Comp
L power:GND #PWR0146
U 1 1 61CC1E7E
P 9125 6400
F 0 "#PWR0146" H 9125 6150 50  0001 C CNN
F 1 "GND" H 9200 6250 50  0000 R CNN
F 2 "" H 9125 6400 50  0001 C CNN
F 3 "" H 9125 6400 50  0001 C CNN
	1    9125 6400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 61CBFFD9
P 9125 6300
F 0 "C2" H 9275 6225 50  0000 R CNN
F 1 "X" H 9275 6350 50  0000 R CNN
F 2 "Spacemanic_SMD:C_0603_HS" H 9125 6300 50  0001 C CNN
F 3 "~" H 9125 6300 50  0001 C CNN
	1    9125 6300
	-1   0    0    1   
$EndComp
Wire Wire Line
	9525 6400 9525 6375
$Comp
L power:GND #PWR0145
U 1 1 61CAAA7C
P 9525 6400
F 0 "#PWR0145" H 9525 6150 50  0001 C CNN
F 1 "GND" H 9600 6250 50  0000 R CNN
F 2 "" H 9525 6400 50  0001 C CNN
F 3 "" H 9525 6400 50  0001 C CNN
	1    9525 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9525 6125 9525 6175
Connection ~ 9525 6125
Wire Wire Line
	9125 6125 9525 6125
Wire Wire Line
	9125 6100 9125 6125
Wire Wire Line
	9525 6075 9525 6125
$Comp
L Device:R_Small R2
U 1 1 61C7845F
P 9525 6275
F 0 "R2" H 9584 6321 50  0000 L CNN
F 1 "200" H 9584 6230 50  0000 L CNN
F 2 "Spacemanic_SMD:R_0603_HS_b" H 9525 6275 50  0001 C CNN
F 3 "~" H 9525 6275 50  0001 C CNN
	1    9525 6275
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 61C7667C
P 9525 5975
F 0 "R1" H 9584 6021 50  0000 L CNN
F 1 "120" H 9584 5930 50  0000 L CNN
F 2 "Spacemanic_SMD:R_0603_HS_b" H 9525 5975 50  0001 C CNN
F 3 "~" H 9525 5975 50  0001 C CNN
	1    9525 5975
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM1117-ADJ U1
U 1 1 61C5657F
P 9125 5800
F 0 "U1" H 9125 6042 50  0000 C CNN
F 1 "LM1117-ADJ" H 9125 5951 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 9125 5800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 9125 5800 50  0001 C CNN
F 4 "LD1117ASTR" H 9125 5800 50  0001 C CNN "MFPN"
	1    9125 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 5000 7300 5325
$Comp
L power:+3.3V #PWR0158
U 1 1 61D6872F
P 4025 4900
F 0 "#PWR0158" H 4025 4750 50  0001 C CNN
F 1 "+3.3V" H 4040 5073 50  0000 C CNN
F 2 "" H 4025 4900 50  0001 C CNN
F 3 "" H 4025 4900 50  0001 C CNN
	1    4025 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0159
U 1 1 61D76179
P 4625 4900
F 0 "#PWR0159" H 4625 4750 50  0001 C CNN
F 1 "+3.3V" H 4640 5073 50  0000 C CNN
F 2 "" H 4625 4900 50  0001 C CNN
F 3 "" H 4625 4900 50  0001 C CNN
	1    4625 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0160
U 1 1 61DBAC9F
P 4525 1125
F 0 "#PWR0160" H 4525 975 50  0001 C CNN
F 1 "+3.3V" H 4540 1298 50  0000 C CNN
F 2 "" H 4525 1125 50  0001 C CNN
F 3 "" H 4525 1125 50  0001 C CNN
	1    4525 1125
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0161
U 1 1 61DF1972
P 5850 1950
F 0 "#PWR0161" H 5850 1800 50  0001 C CNN
F 1 "+3.3V" V 5850 2200 50  0000 C CNN
F 2 "" H 5850 1950 50  0001 C CNN
F 3 "" H 5850 1950 50  0001 C CNN
	1    5850 1950
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0162
U 1 1 61DFFA64
P 6550 1950
F 0 "#PWR0162" H 6550 1800 50  0001 C CNN
F 1 "+3.3V" V 6550 2175 50  0000 C CNN
F 2 "" H 6550 1950 50  0001 C CNN
F 3 "" H 6550 1950 50  0001 C CNN
	1    6550 1950
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0163
U 1 1 61E0D8EC
P 8450 1950
F 0 "#PWR0163" H 8450 1800 50  0001 C CNN
F 1 "+3.3V" V 8450 2175 50  0000 C CNN
F 2 "" H 8450 1950 50  0001 C CNN
F 3 "" H 8450 1950 50  0001 C CNN
	1    8450 1950
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0164
U 1 1 61E1B7DF
P 7750 1950
F 0 "#PWR0164" H 7750 1800 50  0001 C CNN
F 1 "+3.3V" V 7750 2175 50  0000 C CNN
F 2 "" H 7750 1950 50  0001 C CNN
F 3 "" H 7750 1950 50  0001 C CNN
	1    7750 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 5600 6100 5650
Wire Wire Line
	6100 5650 6200 5650
Connection ~ 6200 5650
Wire Wire Line
	6200 5650 6200 5700
Wire Wire Line
	7300 5000 8525 5000
Wire Wire Line
	8525 5000 8525 5800
Wire Wire Line
	8525 5800 8700 5800
Connection ~ 7300 5000
$Comp
L Connector:USB_B_Micro J10
U 1 1 61B48903
P 6200 5200
F 0 "J10" H 6257 5667 50  0000 C CNN
F 1 "USB_B_Micro" H 6257 5576 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 6350 5150 50  0001 C CNN
F 3 "~" H 6350 5150 50  0001 C CNN
F 4 "Molex 105017-0001" H 6200 5200 50  0001 C CNN "MFPN"
	1    6200 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5400 6650 5400
Wire Wire Line
	6650 5400 6650 5825
$Comp
L Connector_Generic:Conn_02x30_Odd_Even J5
U 1 1 5E4EC44F
P 6250 2950
F 0 "J5" H 6300 4475 60  0000 C CNN
F 1 "HEADER_2x30" H 6275 4600 60  0001 C CNN
F 2 "Spacemanic_header:Header_2x30_2.54mm" H 6250 4540 60  0001 C CNN
F 3 "" H 6250 4400 60  0000 C CNN
F 4 "TSW-130-07-x-D" H 6250 2950 50  0001 C CNN "MFPN"
	1    6250 2950
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J9
U 1 1 619D762A
P 4375 5275
F 0 "J9" H 4425 4850 60  0000 C CNN
F 1 "HEADER_2x26" H 4425 5725 60  0001 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x07_P2.54mm_Horizontal" H 4375 6666 60  0001 C CNN
F 3 "" H 4375 6525 60  0000 C CNN
F 4 "SSW-107-02-T-D-RA" H 4375 5275 50  0001 C CNN "MFPN"
	1    4375 5275
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5E6510B7
P 3875 2050
F 0 "#PWR0123" H 3875 1800 50  0001 C CNN
F 1 "GND" H 3950 1900 50  0000 R CNN
F 2 "" H 3875 2050 50  0001 C CNN
F 3 "" H 3875 2050 50  0001 C CNN
	1    3875 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0124
U 1 1 5E6AE908
P 4200 2050
F 0 "#PWR0124" H 4200 1800 50  0001 C CNN
F 1 "GNDA" H 4200 1900 50  0000 C CNN
F 2 "" H 4200 2050 50  0001 C CNN
F 3 "" H 4200 2050 50  0001 C CNN
	1    4200 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J4
U 1 1 5E34ACC1
P 2625 5500
F 0 "J4" H 2675 6850 60  0000 C CNN
F 1 "HEADER_2x26" H 2625 6997 60  0001 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 2625 6891 60  0001 C CNN
F 3 "" H 2625 6750 60  0000 C CNN
F 4 "TSW-126-07-x-D" H 2625 5500 50  0001 C CNN "MFPN"
	1    2625 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J2
U 1 1 5D197155
P 1225 5500
F 0 "J2" H 1275 6975 60  0000 C CNN
F 1 "HEADER_2x26" H 1225 6997 60  0001 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 1225 6891 60  0001 C CNN
F 3 "" H 1225 6750 60  0000 C CNN
F 4 "ESQ-126-14-x-D" H 1225 5500 50  0001 C CNN "MFPN"
	1    1225 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J3
U 1 1 5E34AA3A
P 2625 2450
F 0 "J3" H 2675 3800 60  0000 C CNN
F 1 "HEADER_2x26" H 2625 3947 60  0001 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 2625 3841 60  0001 C CNN
F 3 "" H 2625 3700 60  0000 C CNN
F 4 "TSW-126-07-x-D" H 2625 2450 50  0001 C CNN "MFPN"
	1    2625 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J1
U 1 1 5D194D38
P 1225 2450
F 0 "J1" H 1275 3950 60  0000 C CNN
F 1 "HEADER_2x26" H 1225 3947 60  0001 C CNN
F 2 "Spacemanic_header:Header_2x26_2.54mm" H 1225 3841 60  0001 C CNN
F 3 "" H 1225 3700 60  0000 C CNN
F 4 "ESQ-126-14-x-D" H 1225 2450 50  0001 C CNN "MFPN"
	1    1225 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR0122
U 1 1 5E526D9E
P 3875 1125
F 0 "#PWR0122" H 3875 975 50  0001 C CNN
F 1 "+BATT" H 3750 1300 50  0000 L CNN
F 2 "" H 3875 1125 50  0001 C CNN
F 3 "" H 3875 1125 50  0001 C CNN
	1    3875 1125
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L1
U 1 1 5E702FEF
P 10025 1000
F 0 "L1" V 10100 1000 50  0000 C CNN
F 1 "FB 1000 Ohm 2A" V 9925 675 50  0000 L CNN
F 2 "Inductors_SMD:L_1210_HandSoldering" H 10025 1000 50  0001 C CNN
F 3 "~" H 10025 1000 50  0001 C CNN
F 4 "FBMH3225HM102NT" V 10025 1000 50  0001 C CNN "MFPN"
	1    10025 1000
	0    -1   -1   0   
$EndComp
Text Notes 10425 1425 2    59   ~ 12
Module connector
$Comp
L Connector_Generic:Conn_02x30_Odd_Even J8
U 1 1 619A105D
P 10125 2925
F 0 "J8" H 10175 4575 60  0000 C CNN
F 1 "HEADER_2x30" H 10175 4575 60  0001 C CNN
F 2 "Spacemanic_connector:Molex_SlimStack_receptable_2x30_0.5_2mmStack" H 10125 4515 60  0001 C CNN
F 3 "" H 10125 4375 60  0000 C CNN
F 4 "Molex 54363-0689" H 10125 2925 50  0001 C CNN "MFPN"
	1    10125 2925
	-1   0    0    -1  
$EndComp
Text Notes 8500 1450 2    59   ~ 12
Module connector
$Comp
L Connector_Generic:Conn_02x30_Odd_Even J7
U 1 1 5E463BC1
P 8150 2950
F 0 "J7" H 8200 4600 60  0000 C CNN
F 1 "HEADER_2x30" H 8200 4625 60  0001 C CNN
F 2 "Spacemanic_connector:Molex_SlimStack_receptable_2x30_0.5_2mmStack" H 8150 4540 60  0001 C CNN
F 3 "" H 8150 4400 60  0000 C CNN
F 4 "Molex 54363-0689" H 8150 2950 50  0001 C CNN "MFPN"
	1    8150 2950
	-1   0    0    -1  
$EndComp
Text Notes 1725 1125 2    59   ~ 12
Cubesat bus header
Text Notes 1725 4175 2    59   ~ 12
Cubesat bus header
$EndSCHEMATC
